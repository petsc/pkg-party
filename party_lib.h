/**********************************************************\
*  PARTY PARTITIONING LIBRARY            party_lib.h
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#define VERSION "========== PARTY Version 1.99, 28.10.1998 ===================="

#define VW int

extern int party_lib (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w,
	int p, int *part, int *cutsize,
	int redl, char *redm, char *redo, char *global, char *local, int rec,
	int Output);

extern int global_opt (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part, int *locked, int Output);
extern int global_lin (
	int n, VW *vertex_w, int p, int *part);
extern int global_sca (
	int n, VW *vertex_w, int p, int *part);
extern int global_ran (
	int n, VW *vertex_w, int p, int *part);
extern int global_gbf (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_gcf (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_new (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_bub (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
        int p, int *part, int *seed, int Output);
#ifdef WORK
extern int global_koh (
	int n, VW *vertex_w, float *x, float *y, float *z, 
	int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_flux (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part, int Output);
#endif
extern int global_coo (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int p, int *part);
extern int global_fil (
	int n, int p, int *part, char *filename);

extern int local_kl (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part, int Output);
extern int local_hs (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part, int Output);

#ifdef CHACO
extern int global_cml (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_csm (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_csl (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_cin (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int p, int *part);
extern int local_ckl (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part);
#endif
#ifdef METIS
extern int global_pme (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
extern int global_kme (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part);
#endif

extern int graph_load (
	int *n, VW **vertex_w, float **x, float **y, float **z,
	int **edge_p, int **edge, int **edge_w,
	char *graphfile, char *xyzfile);
extern int graph_save (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w,
	char *graphfile, char *xyzfile);
extern int graph_free (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w);
extern int graph_print (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w);
extern int part_save (
	int n, int *part, char *partfile);

extern int graph_check_and_info (
	int n, VW *vertex_w, float *x, float *y, float *z, 
	int *edge_p, int *edge, int *edge_w, 
	int Output);
extern int cut_size (
	int n, int *edge_p, int *edge, int *edge_w,
	int *part);
extern int part_check (
	int n, VW *vertex_w,
	int p, int *part, int Output);
extern int part_info (
	int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part, int Output);

extern void print_alloc_statistics ();
extern void party_lib_times_start ();
extern void party_lib_times_output (
	int Times);
