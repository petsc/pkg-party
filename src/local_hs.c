/**********************************************************\
*  PARTY PARTITIONING LIBRARY            local_hs.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

static int	N, *Edge_p, *Edge, *Edge_w, *Part;
static VW 	*Vertex_w;
static int 	*in, *ex, *locked, s, bs, S_h[2], *S_list[2], S_part[2], *pairs;
static VW	*part_w, max_vertex_w, S_w[2];
static BUCKET_ELE	*bucket_ele;
static BUCKETS		*B[2];

static long t=0, t_init=0,
  t_pre=0, t_comb=0, t_bip_pre=0, t_bip_pure=0, t_bip_past=0, t_post=0;

void hs_times_start (void)
{ t=t_pre=t_comb=t_bip_pre=t_bip_past=t_bip_pure=t_post=0;
}

void hs_times_output (void)
{ if (t_pre||t_comb||t_bip_pre||t_bip_pure||t_bip_past||t_post)
  { printf("      HS pre            : %.2f\n",(double)t_pre     /1000);
    printf("      HS comb           : %.2f\n",(double)t_comb    /1000);
    printf("      HS bip pre        : %.2f\n",(double)t_bip_pre /1000);
    printf("      HS bip pure       : %.2f\n",(double)t_bip_pure/1000);
    printf("      HS bip past       : %.2f\n",(double)t_bip_past/1000);
    printf("      HS post           : %.2f\n",(double)t_post    /1000);
  }
}

static void build_hs (int s, int limit, int help_min, int diff_min, VW size_min)
{ int j, vertex, neig;

  while (S_h[s]<limit && S_w[s]<size_min && bucket_not_empty(B[s]) && 
	 bucket_max_key(B[s])>=diff_min &&
	 S_h[s]+bucket_max_key(B[s])>=help_min)
  { vertex = (int)(bucket_max_element(B[s]));
    bucket_del (&(bucket_ele[vertex]));
    locked[vertex] = 1;
    S_w[s] += Vertex_w?Vertex_w[vertex]:1;
    S_h[s] += (ex[vertex]-in[vertex]);
    (S_list[s][0])++;
    S_list[s][S_list[s][0]] = vertex;
    for (j=Edge_p[vertex]; j<Edge_p[vertex+1]; j++)
    { neig = Edge[j];
      if (Part[neig]==Part[vertex] && !locked[neig])
      { in[neig] -= Edge_w?Edge_w[j]:1;
	ex[neig] += Edge_w?Edge_w[j]:1;
	if (buckets(bucket_ele[neig]))
          bucket_new_key(&(bucket_ele[neig]),ex[neig]-in[neig]);
        else
	  bucket_in(B[s],&(bucket_ele[neig]),ex[neig]-in[neig]);
} } } }

static void move_logically_back (int s)
{ int i, j, neig, vertex;

  for (i=S_list[s][0]; i>0; i--)
  { vertex = S_list[s][i];
    locked[vertex] = 0;
    if (ex[vertex] > 0)
      bucket_in(B[s],&(bucket_ele[vertex]),ex[vertex]-in[vertex]);
    for (j=Edge_p[vertex]; j<Edge_p[vertex+1]; j++)
    { neig = Edge[j];
      if (Part[neig]==Part[vertex] && !locked[neig])
      { in[neig] += Edge_w?Edge_w[j]:1;
        ex[neig] -= Edge_w?Edge_w[j]:1;
        if (ex[neig] > 0)
	  bucket_new_key(&(bucket_ele[neig]),ex[neig]-in[neig]);
        else
	  bucket_del(&(bucket_ele[neig]));
  } } }
  S_list[s][0] = 0;
}

static void move_physically_over (int from, int to)
{ int i, j, neig, vertex, elements=S_list[from][0];

  for (i=1; i<=elements; i++)
  { vertex = S_list[from][i];
    for (j=Edge_p[vertex]; j<Edge_p[vertex+1]; j++)
    { neig = Edge[j];
      if (Part[neig]==S_part[to])
      { in[neig] += Edge_w?Edge_w[j]:1;
        ex[neig] -= Edge_w?Edge_w[j]:1;
	if (ex[neig] > 0)
	  bucket_new_key(&(bucket_ele[neig]),ex[neig]-in[neig]);
	else
	  bucket_del(&(bucket_ele[neig]));
      }
    }
    INT_CHANGE(in[vertex],ex[vertex]);
    Part[vertex] = S_part[to];
    bucket_in(B[to],&(bucket_ele[vertex]),ex[vertex]-in[vertex]);
    locked[vertex] = 0;
  }
  part_w[S_part[from]] -= S_w[from];
  part_w[S_part[to]] += S_w[from];
}

static void move_physically_back (int from, int to)
{ int i, j, neig, vertex;

  for (i=S_list[from][0]; i>0; i--)
  { vertex = S_list[from][i];
    if (buckets(bucket_ele[vertex]))
      bucket_del(&(bucket_ele[vertex]));
    for (j=Edge_p[vertex]; j<Edge_p[vertex+1]; j++)
    { neig = Edge[j];
      if (Part[neig] == S_part[from])
      { in[neig] += Edge_w?Edge_w[j]:1;
	ex[neig] -= Edge_w?Edge_w[j]:1;
	if (ex[neig] > 0)
	  bucket_new_key(&(bucket_ele[neig]),ex[neig]-in[neig]);
	else
	  bucket_del(&(bucket_ele[neig]));
      }
      else if (Part[neig] == S_part[to])
      { in[neig] -= Edge_w?Edge_w[j]:1;
	ex[neig] += Edge_w?Edge_w[j]:1;
        if (buckets(bucket_ele[neig]))
	  bucket_new_key(&(bucket_ele[neig]),ex[neig]-in[neig]);
        else
	  bucket_in(B[to],&(bucket_ele[neig]),ex[neig]-in[neig]);
    } }
    INT_CHANGE(in[vertex],ex[vertex]);
    bucket_in(B[from],&(bucket_ele[vertex]),ex[vertex]-in[vertex]);
    Part[vertex] = S_part[from];
  }
  part_w[S_part[from]] += S_w[from];
  part_w[S_part[to]] -= S_w[from];
}

static int hs_bisect (int a, int b, int Output)
{ int  	i, j, limit, cutsize=0;
  VW	max_part_w=(part_w[a]+part_w[b]+max_vertex_w)/2;
  
  S_part[0] = a;
  S_part[1] = b;

  for (i=0; i<N; i++)
  { if (Part[i]==a || Part[i]==b)
    { in[i] = ex[i] = 0;
      for (j=Edge_p[i]; j<Edge_p[i+1]; j++)
      { if (Part[Edge[j]]==Part[i])
	  in[i] += (Edge_w?Edge_w[j]:1);
        else if (Part[Edge[j]]==a || Part[Edge[j]]==b)
	  ex[i] += (Edge_w?Edge_w[j]:1);
      }
      if (ex[i] > 0)
      { if (Part[i]==a)
	  bucket_in(B[0], &(bucket_ele[i]), ex[i]-in[i]);
        else
	  bucket_in(B[1], &(bucket_ele[i]), ex[i]-in[i]);
      }
      cutsize += ex[i];
  } }
  cutsize/=2;

  if (Output > 1)
    printf("HS start on %d(%.3f) and %d(%.3f), cutsize=%d\n",a,(double)part_w[a],b,(double)part_w[b],cutsize);
  if (Output > 3)
  { printf("buckets at start:\n");
    bucket_print(B[0]);
    bucket_print(B[1]);
  }
  ADD_NEW_TIME(t_bip_pre);

  limit = (cutsize+1)/2;
  while (limit > 0)
  { if (Output > 3)
      printf("HS Step1 searches for %d-helpful set\n",limit);

    S_h[0] = S_h[1] = S_list[0][0] = S_list[1][0] = 0;
    S_w[0] = S_w[1] = 0;

    if (part_w[S_part[0]]>max_part_w)
      build_hs(0,INT_MAX,INT_MIN,INT_MIN,part_w[a]-max_part_w);
    else if (part_w[S_part[1]]>max_part_w)
      build_hs(1,INT_MAX,INT_MIN,INT_MIN,part_w[b]-max_part_w);
    else
    { build_hs (0, limit, INT_MIN, 0, INT_MAX);
      if (S_h[0]<limit)
        build_hs (1, limit, INT_MIN, 0, INT_MAX);
    }

    if (Output > 3)
      printf("H(S0):%d W(S0):%f |S0|:%d, H(S1):%d W(S1):%f |S1|:%d\n",S_h[0],(double)S_w[0],S_list[0][0],S_h[1],(double)S_w[1],S_list[1][0]);

    if (S_h[0]>0 || S_h[1]>0 || part_w[a]>max_part_w || part_w[b]>max_part_w)
    { if (part_w[a]>max_part_w || (part_w[b]<=max_part_w && (S_h[0]>S_h[1] || (S_h[0]==S_h[1]&&S_w[0]<S_w[1]))))
      { s = 0;
	bs = 1;
      }
      else
      { s = 1;
	bs = 0;
      }
      move_logically_back (bs);
      limit = MIN(limit,S_h[s]);
    }
    else
    { move_logically_back (0);
      move_logically_back (1);
      limit = 0;
      s = bs = 0;
      if (Output > 2)
        puts("HS Step1  no helpful set found");
    }

    if (s != bs)
    { cutsize -= S_h[s];
      if (Output > 3)
        printf("HS Step1 found S...H(S)=%d, W(S)=%.3f, |S|=%d\n",S_h[s],(double)S_w[s],S_list[s][0]);

      move_physically_over (s, bs); 

      if (Output > 2)
        printf("HS Step1 try w=%.3f/%.3f and cutsize=%d\n",(double)part_w[a],(double)part_w[b],cutsize);

      S_h[bs] = S_list[bs][0] = 0;
      S_w[bs] = 0;
      build_hs(bs,INT_MAX,-(S_h[s])+1,INT_MIN,part_w[S_part[bs]]-max_part_w);

      if (Output > 3)
	printf("H(S0):%d W(S0):%f |S0|:%d, H(S1):%d W(S1):%f |S1|:%d\n",S_h[0],(double)S_w[0],S_list[0][0],S_h[1],(double)S_w[1],S_list[1][0]);

      if (MAX(part_w[S_part[s]]+S_w[bs],part_w[S_part[bs]]-S_w[bs]) <= MAX(max_part_w,MAX(part_w[S_part[s]]+S_w[s],part_w[S_part[bs]]-S_w[s])))
      { move_physically_over (bs, s);
        cutsize -= (S_h[bs]);
	limit *= 2;
	limit = MIN(limit,cutsize/2);
        if (Output > 3)
          printf("HS Step2 found BS...H(BS)=%d, W(BS)=%.3f, |BS|=%d\n",S_h[bs],(double)S_w[bs],S_list[bs][0]);
        if (Output > 2)
          printf("HS Step2 new w=%.3f/%.3f and cutsize %d\n",(double)part_w[a],(double)part_w[b],cutsize);
      }
      else
      { move_logically_back (bs);
	cutsize += (S_h[s]);
	move_physically_back (s, bs);
        limit /= 2;
        if (Output > 2)
          puts("HS Step2  no success");
  } } }
  ADD_NEW_TIME(t_bip_pure);

  if (Output > 1)
    printf("HS end   on %d(%.3f) and %d(%.3f), cutsize=%d\n",a,(double)part_w[a],b,(double)part_w[b],cutsize);
  if (Output > 3)
  { printf("buckets at end:\n");
    bucket_print(B[0]);
    bucket_print(B[1]);
  }

  bucket_clear(B[0]);
  bucket_clear(B[1]);
  ADD_NEW_TIME(t_bip_past);
  return 0;
}

int local_hs (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part, int Output)
{ int		i, j, degree_weighted, maxdegree=0, pairs_done=0;
  VW		total_w=0, max_part_w=0;

  INIT_TIME();

  if (p>2)
    WARNING("LOCAL_HS", "local_hs draft version for p>2");

  N=n; Vertex_w=vertex_w; Edge_p=edge_p; Edge=edge; Edge_w=edge_w; Part=part;
  max_vertex_w = 1;

  CALLOC ("LOCAL_HS",in,int,n);
  CALLOC ("LOCAL_HS",ex,int,n);
  CALLOC ("LOCAL_HS",locked,int,n);
  CALLOC ("LOCAL_HS",part_w,VW,p);
  CALLOC ("LOCAL_HS",S_list[0],int,n+1);
  CALLOC ("LOCAL_HS",S_list[1],int,n+1);
  CALLOC ("LOCAL_HS",bucket_ele,BUCKET_ELE,n);
  CALLOC ("LOCAL_HS",pairs,int,p*p);

  WEIGHTS(total_w,max_part_w,max_vertex_w,n,vertex_w,p);

  for (i=0; i<n; i++)
  { bucket_ele[i].element = (void*)i;
    if (part[i]<0 || part[i]>=p)
    { fprintf(stderr, "LOCAL_HS ERROR...part[%d]=%d not in range 0..%d!\n",i,part[i],p-1);
      return 1;
    }
    part_w[part[i]] += (vertex_w?vertex_w[i]:1);
    if (edge_w)
    { degree_weighted = 0;
      for (j=edge_p[i]; j<edge_p[i+1]; j++)
        degree_weighted += edge_w[j];
      maxdegree = MAX(maxdegree,degree_weighted);
    }
    else
      maxdegree = MAX(maxdegree,edge_p[i+1]-edge_p[i]);
  }
  if (bucket_cal(&(B[0]),-maxdegree, maxdegree))
    FAILED ("LOCAL_HS", "bucket_cal");
  if (bucket_cal(&(B[1]),-maxdegree, maxdegree))
    FAILED ("LOCAL_HS", "bucket_cal");
  if (Output > 0)
  { VW max_w = 0;
    for (i=0; i<p; i++)
      max_w = MAX(max_w,part_w[i]);
    printf("HS START... max. w: %.2f (<=%.2f)  cutsize: %3d\n", (double)max_w, (double)max_part_w, cut_size(n,edge_p,edge,edge_w,part));
    for (i=0; i<p; i++)
      if (part_w[i] > max_part_w)
      { WARNING ("LOCAL_HS","unbalanced initial partition");
        printf("weight of part %d = %.2f too high (<=%.2f)!\n",i,(double)(part_w[i]),(double)max_part_w);
      }
  }
  ADD_NEW_TIME(t_pre);

  for (i=0; pairs_done<p*(p-1)/2; i=(i+1)%p)
  { for (j=i+1; j<p && pairs_done<p*(p-1)/2; j++)
    { if (pairs[i*p+j])
	pairs_done++;
      else
      { if (hs_bisect(i,j,Output))
	  FAILED ("LOCAL_HS", "hs_bisect");
        pairs[i*p+j] = 1;
	pairs_done = 1;
      }
    }
  }
  ADD_NEW_TIME(t_comb);

  if (Output > 0)
  { VW max_w = 0;
    for (i=0; i<p; i++)
      max_w = MAX(max_w,part_w[i]);
    printf("HS END  ... max. weight: %.2f (<=%.2f)  cutsize: %3d \n", (double)max_w, (double)max_part_w, cut_size(n,edge_p,edge,edge_w,part));
  }
  bucket_free (B[0]);
  bucket_free (B[1]);
  FREE (in,int,n);
  FREE (ex,int,n);
  FREE (locked,int,n);
  FREE (part_w,VW,p);
  FREE (S_list[0],int,n+1);
  FREE (S_list[1],int,n+1);
  FREE (bucket_ele,BUCKET_ELE,n);
  FREE (pairs,int,p*p); 
  ADD_NEW_TIME(t_post);
  END_TIME();
  return 0;
}
