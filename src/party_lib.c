/**********************************************************\
*  PARTY PARTITIONING LIBRARY            party_lib.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"
int     Xpart, *id;
char    xpart_c[100];

static long t=0, t_init=0,
	t_global, t_local=0, t_match=0,
	t_match_opt=0, t_red=0, t_part_rest=0;

void party_lib_times_start ()
{ void hs_times_start ();
  void kl_times_start ();

  hs_times_start ();
  kl_times_start ();
  t_global=t_local=t_part_rest=t_match=t_match_opt=t_red = 0;
}

void party_lib_times_output (int Times)
{ void hs_times_output ();
  void kl_times_output ();

  if (Times > 0)
  { printf("    Matching          : %.2f\n",(double)t_match/1000);
    printf("    Matching-Opt      : %.2f\n",(double)t_match_opt/1000);
    printf("    Reduce            : %.2f\n",(double)t_red/1000);
    printf("    Global            : %.2f\n",(double)t_global/1000);
    printf("    Local             : %.2f\n",(double)t_local/1000);
    if (Times > 1)
    { hs_times_output();
      kl_times_output();
    }
    printf("    Part-rest         : %.2f\n",(double)t_part_rest/1000);
  }
}

int party_lib (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w,
	int p, int *part, int *cutsize,
	int redl, char *redm, char *redo, char *global, char *local, int rec,
	int Output)
{ int	i, error=0;
  char	redm_def[10]="lam", redo_def[10]="w3", global_def[10]="def",
	local_def[10]="hs";

  INIT_TIME();
  ADD_NEW_TIME(t_part_rest);
  if (!part)		ERROR ("PARTY_LIB", "'part' parameter is NULL");
  if (redl < p)		redl = p;
  if (!redm)		redm = redm_def;
  if (!redo)		redo = redo_def;
  if (!global)		global = global_def;
  if (!local)		local = local_def;
  if (rec)
  { int pp=1;
    while (pp < p )
      pp = 2*pp;
    if (pp != p)
    { fprintf(stderr,"PARTY_LIB ERROR...p=%d has to be 2^a in rec. mode.\n",p);
      return 1;
  } }

  if (Output > 0)
    printf("PARTY_LIB START %d/%s/%s-%s/%s/%d- on n=%d,e=%d in p=%d...\n",
	   redl,redm,redo,global,local,rec,n,edge_p[n],p);
  if (Output>2 && graph_check_and_info(n,vertex_w,x,y,z,edge_p,edge,edge_w,Output-2))
    FAILED ("PARTY_LIB", "graph_check_and_info");

  if (!srand_set)
  { srand ((long)RANDOM_SEED);
    srand_set = 1;
  }
  
  if (p <= 0)
  { fprintf(stderr, "PARTY_LIB ERROR...p=%d too small!\n",p);
    return 1;
  }
  else if (p == 1)
  { for (i=0; i<n; i++)
      part[i] = 0;
    (*cutsize) = 0;
  }
  else if (p >= n)
  { if (p > n)
      fprintf(stderr, "PARTY_LIB WARNING...p=%d > n=%d!\n",p,n);
    for (i=0; i<n; i++)
      part[i] = i;
    if (edge_w)
    { (*cutsize) = 0;
      for (i=0; i<edge_p[n]; i++)
        (*cutsize) += edge_w[i];
    }
    else
      (*cutsize) = edge_p[n];
    (*cutsize) /= 2;
  }
  else if (n > redl  &&  edge_p[n] > 0)
  { int		i, red_n=0, *red_part, *red_edge_p, *red_edge, *red_edge_w,
		*matching;
    VW		*red_vertex_w;
    float	*red_x=NULL, *red_y=NULL, *red_z=NULL;

    CALLOC ("PARTY_LIB", matching, int, n);
    ADD_NEW_TIME(t_part_rest);

    if (!strcmp(redm, "lam"))
    { if (matching_lam (n,vertex_w,edge_p,edge,edge_w,redl,matching,&red_n))
        FAILED ("PARTY_LIB", "matching_lam");
    }
    else if (strcmp(redm,"no"))
    { fprintf(stderr,"PARTY_LIB ERROR...unknown redm '%s'\n",redm);
      return 1;
    }
    if (Output > 1)
      printf("Matching-Ratio: %.2f\n",(double)2*(n-red_n)/n);
    ADD_NEW_TIME(t_match);

    if (!strcmp(redo, "w3"))
    { if (matching_opt_w3 (n,vertex_w,edge_p,edge,edge_w,matching,&red_n,redl))
        FAILED ("PARTY_LIB", "matching_opt");
    }
    else if (strcmp(redo,"no"))
    { fprintf(stderr,"PARTY_LIB ERROR...unknown redo '%s'\n",redo);
      return 1;
    }
    if (Output > 1)
      printf("New Matching-Ratio: %.2f\n",(double)2*(n-red_n)/n);
    ADD_NEW_TIME(t_match_opt);

    MALLOC ("PARTY_LIB", red_vertex_w, VW, red_n);
    MALLOC ("PARTY_LIB", red_edge_p, int, red_n+1);
    MALLOC ("PARTY_LIB", red_edge, int, edge_p[n]);
    MALLOC ("PARTY_LIB", red_edge_w, int, edge_p[n]);
    CALLOC ("PARTY_LIB", red_part, int, red_n);
    if (x)
      MALLOC ("PARTY_LIB", red_x, float, red_n);
    if (y)
      MALLOC ("PARTY_LIB", red_y, float, red_n);
    if (z)
      MALLOC ("PARTY_LIB", red_z, float, red_n);
    if (graph_reduce(n,vertex_w,x,y,z,edge_p,edge,edge_w,red_n,red_vertex_w,
	red_x,red_y,red_z,red_edge_p,red_edge,red_edge_w,matching,part,red_part))
      FAILED ("PARTY_LIB", "reduce");
    ADD_NEW_TIME(t_red);

    if (party_lib (red_n,red_vertex_w,red_x,red_y,red_z,red_edge_p,red_edge,
	red_edge_w,p,red_part,cutsize,redl,redm,redo,global,local,rec,Output))
      FAILED ("PARTY_LIB", "party_lib");

    for (i=0; i<n; i++)
      part[i] = red_part[part[i]];

    FREE (red_vertex_w, VW, red_n);
    FREE (red_edge_p, int, red_n+1);
    FREE (red_edge, int, edge_p[n]);
    FREE (red_edge_w, int, edge_p[n]);
    FREE (red_part, int, red_n);
    FREE (red_x, float, red_n);
    FREE (red_y, float, red_n);
    FREE (red_z, float, red_n);
    FREE (matching, int, n);

    if (Output > 1)
      printf("PARTY_LIB red to cutsize %5d \n", cut_size(n,edge_p,edge,edge_w,part));
    ADD_NEW_TIME(t_part_rest);

    if (!strcmp(local,"kl"))
      error = local_kl(n,vertex_w,edge_p,edge,edge_w,p,part,Output-3);
    else if (!strcmp(local,"hs"))
      error = local_hs(n,vertex_w,edge_p,edge,edge_w,p,part,Output-3);
#ifdef CHACO
    else if (!strcmp(local,"ckl"))
      error = local_ckl(n,vertex_w,edge_p,edge,edge_w,p,part);
#endif
    else if (strcmp(local,"no"))
    { fprintf(stderr,"PARTY_LIB ERROR...unknown local method '%s'\n",local);
      return 1;
    }
    if (error)
    { fprintf(stderr,"PARTY_LIB ERROR...failed local %s\n", local);
      return 1;
    }
    (*cutsize) = cut_size(n,edge_p,edge,edge_w,part);
    ADD_NEW_TIME(t_local);
  }
  else
  { int   parts=p, *part_best=NULL, cut_best=INT_MAX;
    char  globals[100], *globals_left, Global[100];

    if (rec)
      parts = 2;

    sprintf(globals,"%s",global);
    globals_left = globals;
    while (globals_left)
    { if (!strncmp(globals_left,"def",3))
      { sprintf(Global,"%s",globals_left+3);
        if (x)
          sprintf(globals_left,"lin,ran,gbf,gcf,coo%s",Global);
        else
	  sprintf(globals_left,"lin,ran,gbf,gcf%s",Global);
      }
      strncpy (Global, globals_left, strcspn(globals_left,","));
      Global[strcspn(globals_left,",")] = 0;
      globals_left = strpbrk (globals_left, ",");
      if (globals_left)
        globals_left++;

      ADD_NEW_TIME(t_part_rest);
      if (!strcmp(Global,"lin"))
	error = global_lin(n,vertex_w,parts,part);
      else if (!strcmp(Global,"sca"))
	error = global_sca(n,vertex_w,parts,part);
      else if (!strcmp(Global,"ran"))
	error = global_ran(n,vertex_w,parts,part);
      else if (!strcmp(Global,"coo"))
	error = global_coo(n,vertex_w,x,y,z,parts,part);
      else if (!strcmp(Global,"gbf"))
	error = global_gbf(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"gcf"))
        error = global_gcf(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"opt"))
	error = global_opt(n,vertex_w,edge_p,edge,edge_w,parts,part,NULL,Output);
      else if (!strcmp(Global,"bub"))
	error = global_bub(n,vertex_w,edge_p,edge,edge_w,parts,part,NULL,Output-3);
#ifdef WORK
      else if (!strcmp(Global,"koh"))
	error = global_koh(n,vertex_w,x,y,z,edge_p,edge,edge_w,parts,part);
#endif
#ifdef CHACO
      else if (!strcmp(Global,"cml"))
        error = global_cml(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"csm"))
        error = global_csm(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"csl"))
        error = global_csl(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"cin"))
        error = global_cin(n,vertex_w,x,y,z,parts,part);
#endif
#ifdef METIS
      else if (!strcmp(Global,"pme"))
        error = global_pme(n,vertex_w,edge_p,edge,edge_w,parts,part);
      else if (!strcmp(Global,"kme"))
        error = global_kme(n,vertex_w,edge_p,edge,edge_w,parts,part);
#endif
      else if (strcmp(Global,"par"))
        error = global_fil(n,parts,part,Global);
      if (error)
      { fprintf(stderr,"PARTY_LIB ERROR...failed global %s\n", Global);
        return 1;
      }
      ADD_NEW_TIME(t_global);

      if (Output > 1)
        printf("PARTY_LIB global %s to cutsize %5d \n",Global, cut_size(n,edge_p,edge,edge_w,part));
      sprintf (xpart_c, "%s",Global);
      XPART(1,"PARTY_LIB", part);
      ADD_NEW_TIME(t_part_rest);

      if (!strcmp(local,"kl"))
        error = local_kl(n,vertex_w,edge_p,edge,edge_w,parts,part,Output-3);
      else if (!strcmp(local,"hs"))
        error = local_hs(n,vertex_w,edge_p,edge,edge_w,parts,part,Output-3);
#ifdef CHACO
      else if (!strcmp(local,"ckl"))
        error = local_ckl(n,vertex_w,edge_p,edge,edge_w,parts,part);
#endif
      else if (strcmp(local,"no"))
      { fprintf(stderr,"PARTY_LIB ERROR...unknown local method '%s'\n",local);
        return 1;
      }
      if (error)
      { fprintf(stderr,"PARTY_LIB ERROR...failed local %s\n", local);
        return 1;
      }
      ADD_NEW_TIME(t_local);
     
      (*cutsize) = cut_size(n,edge_p,edge,edge_w,part);
      if (Output > 1)
        printf("PARTY_LIB local  %5s to cutsize %6d \n",local, *cutsize);
      sprintf(xpart_c, "%s+%s", Global, local);
      XPART(1,"PARTY_LIB",part);

      if (globals_left && (!part_best || PARTY_OPT((*cutsize))<PARTY_OPT(cut_best)))
      { if (!part_best)
          MALLOC("PARTY_LIB",part_best,int,n);
	memcpy(part_best,part,n*sizeof(int)); 
	cut_best = (*cutsize);
      }
    }

    if (part_best)
    { if (PARTY_OPT(cut_best)<PARTY_OPT((*cutsize)))
      { memcpy(part,part_best,n*sizeof(int));
        (*cutsize) = cut_best;
      }
      FREE (part_best,int,n);
    }

    if (rec && p>2)
    { int 	i, j, k, *changes, n_[2], e_[2], cutsize_[2], 
		*edge_p_[2], *edge_[2], *edge_w_[2], *part_[2]; 
      VW	*vertex_w_[2];
      float	*x_[2], *y_[2], *z_[2];
      int	*id_[2], *id_save=id;

      for (i=0; i<2; i++)
      { n_[i] = e_[i] = 0;
        edge_p_[i]=edge_[i]=edge_w_[i]=part_[i]=id_[i] = NULL;
        vertex_w_[i] = NULL;
	x_[i]=y_[i]=z_[i] = NULL;
      }

      for (i=0; i<n; i++)
      { n_[part[i]]++;
        for (j=edge_p[i]; j<edge_p[i+1]; j++)
          if (part[edge[j]] == part[i])
            e_[part[i]]++;
      }

      for (k=0; k<2; k++)
      { if (vertex_w)
          MALLOC("PARTY_LIB",vertex_w_[k],VW,n_[k]);
        if (x)
          MALLOC("PARTY_LIB",x_[k],float,n_[k]);
        if (y)
          MALLOC("PARTY_LIB",y_[k],float,n_[k]);
        if (z)
          MALLOC("PARTY_LIB",z_[k],float,n_[k]);
        MALLOC("PARTY_LIB",edge_p_[k],int,(n_[k])+1);
	edge_p_[k][0] = 0;
        MALLOC("PARTY_LIB",edge_[k],int,e_[k]);
        MALLOC("PARTY_LIB",part_[k],int,n_[k]);
        if (edge_w)
          MALLOC("PARTY_LIB",edge_w_[k],int,e_[k]);
        if (Xpart>0 && id)
          CALLOC("PARTY_LIB",id_[k],int,n_[k]);
        n_[k] = e_[k] = 0;
      }
      CALLOC("PARTY_LIB",changes,int,n);
	      
      for (i=0; i<n; i++)
      { changes[i] = n_[part[i]];
        if (Xpart>0 && id)
	  id_[part[i]][n_[part[i]]] = id[i];
        if (vertex_w)
          vertex_w_[part[i]][n_[part[i]]] = vertex_w[i];
        if (x)
          x_[part[i]][n_[part[i]]] = x[i];
        if (y)
	  y_[part[i]][n_[part[i]]] = y[i];
        if (z)
	  z_[part[i]][n_[part[i]]] = z[i];
        for (j=edge_p[i]; j<edge_p[i+1]; j++)
          if (part[edge[j]]==part[i])
	  { if (edge_w)
	      edge_w_[part[i]][e_[part[i]]] = edge_w[j];
	    edge_[part[i]][(e_[part[i]])++] = edge[j];
	  }
        (n_[part[i]])++;
        edge_p_[part[i]][n_[part[i]]] = e_[part[i]];
      }
      for (k=0; k<2; k++)
        for (i=0; i<e_[k]; i++)
          edge_[k][i] = changes[edge_[k][i]];

      for (k=0; k<2; k++)
      { if (Xpart>0)
          id = id_[k];
        if (party_lib(n_[k],vertex_w_[k],x_[k],y_[k],z_[k],edge_p_[k],edge_[k],
		      edge_w_[k],p/2,part_[k],&(cutsize_[k]),
		      redl,redm,redo,global,local,rec,Output))
          FAILED ("PARTY_LIB", "party_lib");
        (*cutsize) += cutsize_[k];
      }

      for (i=0; i<n; i++)
        part[i] = part[i]*p/2 + part_[part[i]][changes[i]];

      for (k=0; k<2; k++)
      { FREE (vertex_w_[k],VW,n_[k]);
        FREE (x_[k],float,n_[k]);
        FREE (y_[k],float,n_[k]);
        FREE (z_[k],float,n_[k]);
        FREE (edge_p_[k],int,(n_[k])+1);
        FREE (edge_[k],int,e_[k]);
        FREE (edge_w_[k],int,e_[k]);
        FREE (part_[k],int,n_[k]);
        if (Xpart>0 && id)
        { id = id_save;
  	  FREE (id_[k],int,n_[k]);
        }
      }
      FREE (changes,int,n);
    }
  }
/*
  if (x && y && !z)
  { char file[200], xyz[200], partf[200];
    sprintf(file,"./XPART/graphs/test%d.chaco",n);
    sprintf(xyz,"./XPART/graphs/test%d.xyz",n);
    sprintf(partf,"./XPART/graphs/test%d.p",n);
    graph_save (file,xyz,n,vertex_w,x,y,z,edge_p,edge,edge_w);
    part_save (partf,n,part);
  }
*/
  sprintf (xpart_c, "Final");
  XPART(1,"PARTY_LIB",part);
  if (Output>1 && part_check(n,vertex_w,p,part,Output-1))
    FAILED ("PARTY_LIB", "part_check");
  if (Output>2 && part_info (n,vertex_w,edge_p,edge,edge_w,p,part,Output-1))
    FAILED ("PARTY_LIB", "part_info");
  if (Output > 0)
    printf("PARTY_LIB FINAL %d/%s/%s-%s/%s/%d- on n=%d,e=%d in p=%d cut=%d\n",
	   redl,redm,redo,global,local,rec,n,edge_p[n],p,*cutsize);
  ADD_NEW_TIME(t_part_rest);
  END_TIME();
  return 0;
}
