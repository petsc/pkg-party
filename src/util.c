/**********************************************************\
*  PARTY PARTITIONING LIBRARY            util.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

#if !defined(CLOCK)
#if !defined(WIN)
struct rusage    Rusage;
#endif
#endif

int	alloc_memory=0, max_memory=0;
int	srand_set = 0;

void print_alloc_statistics (void)
{ printf("MEMORY (current/max): %d / %d\n",alloc_memory,max_memory);
}


static int graph_check (int n, VW *vertex_w, int *edge_p, int *edge, 
	int *edge_w)
{ int	i, j, k, neig, error=0, *neighbors;

  CALLOC ("GRAPH_CHECK",neighbors,int,n);

  for (i=0; i<n; i++)
  { for (j=edge_p[i]; j<edge_p[i+1]; j++)
    { neig = edge[j];
      if (neig<0 || neig>=n)
      { printf("GRAPH_CHECK ERROR...neighbor %d of vertex %d is out of range [%d,%d]\n",neig,i,0,n-1);
	error=1;
      }
      if (neig == i)
      { printf("GRAPH_CHECK ERROR...selfloop of %d\n",i);
	error=1;
      }
      neighbors[neig] ++;
      for (k=edge_p[neig]; k<edge_p[neig+1] && edge[k]!=i; k++);
      if (k == edge_p[neig+1])
      { printf("GRAPH_CHECK ERROR...single directed edge (%d,%d)\n",i,neig);
        error=1;
      }
      else if (edge_w && edge_w[j] != edge_w[k])
      { printf("GRAPH_CHECK ERROR...weighted edge (%d,%d) has different weights %d and %d\n",i,neig,edge_w[j], edge_w[k]);
        error=1;
      }
    }
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
    { if (neighbors[edge[j]] > 1)
      { printf("GRAPH_CHECK ERROR...double edge (%d,%d)\n",i,edge[j]);
	error=1;
      }
      neighbors[edge[j]] = 0; 
    }
  }

  if (vertex_w)
    for (i=0; i<n; i++)
      if (vertex_w[i]<=0.0)
      { printf("GRAPH_CHECK ERROR...vertex %d has weight %f!\n",i,(double)vertex_w[i]);
	error=1;
      }
  if (edge_w)
    for (i=0; i<edge_p[n]; i++)
      if (edge_w[i]<=0)
      { printf("GRAPH_CHECK ERROR...edge %d has weight %d!\n",i,edge_w[i]);
        error=1;
      }

  FREE(neighbors,int,n);
  return error;
}

int graph_check_and_info (int n, VW *vertex_w, float *x, float *y, 
	float *z, int *edge_p, int *edge, int *edge_w, int Output)
{ int  	i, j, *locked, *queue, components, first_unlocked,
	edge_w_min=INT_MAX, edge_w_max=0, edge_w_tot=0, 
	degree_min=INT_MAX, degree_max=0, degree_tot=0,
	degree_w_min=INT_MAX, degree_w_max=0, degree_w_tot=0,
	edge_weight, degree, degree_weighted;
  VW	vertex_weight, vertex_w_min=INT_MAX, vertex_w_max=0, vertex_w_tot=0;

  if (graph_check(n,vertex_w,edge_p,edge,edge_w))
    FAILED ("GRAPH_CHECK_AND_INFO", "graph_check");

  if (Output <= 0)
    return 0;
  puts("---------- Graph Information (min/ave/max/tot) --------------------");

  if ( n<= 0)
  { printf("Graph empty (n=%d)!\n",n);
    return 0;
  }

  for (i=0; i<n; i++)
  { vertex_weight = vertex_w?vertex_w[i]:1;
    degree = edge_p[i+1] - edge_p[i];
  
    vertex_w_tot += vertex_weight;
    vertex_w_min = MIN(vertex_w_min,vertex_weight);
    vertex_w_max = MAX(vertex_w_max,vertex_weight);

    degree_tot += degree;
    degree_min = MIN(degree_min,degree);
    degree_max = MAX(degree_max,degree);

    degree_weighted = 0;
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
    { edge_weight = edge_w?edge_w[j]:1;
      degree_weighted += edge_weight;
      edge_w_min = MIN(edge_w_min,edge_weight);
      edge_w_max = MAX(edge_w_max,edge_weight);
    }
    edge_w_tot += degree_weighted;

    degree_w_tot += degree_weighted;
    degree_w_min = MIN(degree_w_min,degree_weighted);
    degree_w_max = MAX(degree_w_max,degree_weighted);
  }

  printf("# Vertices/Edges    : %d / %d\n",n,degree_tot/2);
  if (vertex_w_min != 1  ||  vertex_w_max != 1)
    printf("Vertex weights      :    %5.2f %7.2f %5.2f %9.2f\n",(double)vertex_w_min,(double)vertex_w_tot/n,(double)vertex_w_max,(double)vertex_w_tot);
  if (edge_w_min != 1  ||  edge_w_max != 1)
    printf("Edge weights        :    %5d %7.2f %5d %9d\n",edge_w_min,(double)edge_w_tot/degree_tot,edge_w_max,edge_w_tot/2);
  printf("Degree              :    %5d %7.2f %5d %9d\n",degree_min,(double)degree_tot/n,degree_max,degree_tot);
  if (edge_w_min != 1  ||  edge_w_max != 1)
    printf("Degree weights      :    %5d %7.2f %5d %9d\n",degree_w_min,(double)degree_w_tot/n,degree_w_max,degree_w_tot);

  if (x)
  { float min=x[0], max=x[0];
    printf("Intervals of coord. :");
    for (i=1; i<n; i++)
    { min = MIN(min,x[i]);
      max = MAX(max,x[i]);
    }
    printf(" [%.2f,%.2f]",min,max);
    if (y)
    { float min=y[0], max=y[0];
      for (i=1; i<n; i++)
      { min = MIN(min,y[i]);
        max = MAX(max,y[i]);
      }
      printf(" [%.2f,%.2f]",min,max);
      if (z)
      { float min=z[0], max=z[0];
        for (i=1; i<n; i++)
        { min = MIN(min,z[i]);
          max = MAX(max,z[i]);
        }
        printf(" [%.2f,%.2f]",min,max);
    } }
    printf("\n");
  }

  /* Diameter and Girth */
  if (Output > 2)
  { int diameter=0, girth=n, *dist, *queue, queue_head, queue_tail;
    MALLOC ("GRAPH_CHECK_AND_INFO",dist,int,n);
    MALLOC ("GRAPH_CHECK_AND_INFO",queue,int,n);
    if (Output > 10)
    { for (i=0; i<n && diameter >=0; i++)
      { for (j=0; j<n; j++)
	  dist[j] = -1;
        dist[i] = 0;
        queue[0] = i;
        queue_head = queue_tail = 0;
        while (queue_head<=queue_tail)
        { int vertex = queue[queue_head++];
	  for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
	    if (dist[edge[j]] == -1)
	    { queue[++queue_tail] = edge[j];
	      dist[edge[j]] = dist[vertex]+1;
            }
        }
        if (queue_tail < n-1)
	  diameter = -1;
        else
	  diameter = MAX(diameter,dist[queue[n-1]]);
      }
      if (diameter == -1)
        printf("Diameter            : infinite\n");
      else
        printf("Diameter            : %d\n",diameter);
    }
    for (i=0; i<n; i++)
      dist[i] = queue[i] = 0;
    for (i=0; i<n && girth>3; i++) 
    { queue[0] = i;
      queue_head = queue_tail = 0;
      dist[i] = 1;  /* Root becomes distance 1 */
      while (queue_head<=queue_tail && 2*dist[queue[queue_head]]-1<girth)
      { int vertex = queue[queue_head++];
	for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
	  if (dist[edge[j]] == 0)
	  { queue[++queue_tail] = edge[j];
	    dist[edge[j]] = dist[vertex]+1;
          }
	  else if (dist[edge[j]]>0 && dist[vertex]+dist[edge[j]]-1<girth)
	    girth = dist[vertex]+dist[edge[j]]-1;
	dist[vertex] = -1;
      }
      for (j=0; j<=queue_tail; j++)
	dist[queue[j]] = 0;
    }
    printf("Girth               : %d\n",girth);
    FREE(dist,int,n);
    FREE(queue,int,n);
  }

  /* Components */
  CALLOC ("GRAPH_CHECK_AND_INFO",locked,int,n);
  MALLOC ("GRAPH_CHECK_AND_INFO",queue,int,n);
  i = components = first_unlocked = 0;
  while (i<n)
  { int k, size=1, queue_head=0, queue_tail=1, vertex;
    components++;
    while (locked[first_unlocked])
      first_unlocked++;
    queue[0] = first_unlocked;
    locked[first_unlocked] = 1;
    while (queue_head < queue_tail)
    { vertex = queue[queue_head++];
      for (k=edge_p[vertex]; k<edge_p[vertex+1]; k++)
        if (!(locked[edge[k]]))
        { size++;
	  locked[edge[k]] = 1;
          queue[queue_tail++] = edge[k]; 
        }
    }
    if (Output > 1)
      printf ("Component %d size    : %d\n", components, size);
    i += size;
  }
  printf("Components          : %d\n",components);
  FREE(locked,int,n);
  FREE(queue,int,n);

  puts("-------------------------------------------------------------------");
  return 0;
}

int cut_size (int n, int *edge_p, int *edge, int *edge_w, int *part)
{ int   i, j, cuts=0, part_i;

  if (edge_w)
  { for (i=0; i<n; i++)
    { part_i = part[i];
      for (j=edge_p[i]; j<edge_p[i+1]; j++)
        if (part_i != part[edge[j]])
          cuts += (edge_w[j]);
  } }
  else
  { for (i=0; i<n; i++)
    { part_i = part[i];
      for (j=edge_p[i]; j<edge_p[i+1]; j++)
        if (part_i != part[edge[j]])
          cuts++;
  } }
  return cuts/2;
}

int part_check (int n, VW *vertex_w, int p, int *part, int Output)
{ int            i;
  VW             *weight, total_weight=0, max_part_weight=0,
                 min_weight=(VW)INT_MAX, max_weight=0;

  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);
  CALLOC ("MAX_LOAD",weight,VW,p);

  if (vertex_w)
    for (i=0; i<n; i++)
      weight[part[i]] += (vertex_w[i]);
  else
    for (i=0; i<n; i++)
      weight[part[i]] += 1;

  if (Output > 0)
    printf("load:");
  for (i=0; i<p; i++)
  { total_weight += weight[i];
    min_weight = MIN(min_weight,weight[i]);
    max_weight = MAX(max_weight,weight[i]);
    if (Output > 1) 
      printf(" %d:%.1f",i, (double)weight[i]);
  }
  FREE(weight,VW,p);

  if (Output > 0)
    printf(" [%.1f,%.1f] (ave.: %.1f)\n",(double)min_weight,(double)max_weight,(double)total_weight/p);
  if (max_weight > max_part_weight)
  { fprintf(stderr, "MAX_LOAD ERROR...part weight of %.1f too high, should be <%.1f!\n",(double)max_weight, (double)max_part_weight);
  }
  return 0;
}

static int min_max (int P, int *l, int Output)
{ int   i, values[3];

  if (P > 0)
  { values[0] = INT_MAX;
    values[1] = values[2] = 0;
    for (i=0; i<P; i++)
    { if (Output > 2)
        printf ("%4d ",l[i]);
      values[2] += l[i];
      values[0] = MIN(values[0],l[i]);
      values[1] = MAX(values[1],l[i]);
    }
    if (Output > 0)
      printf("%9d %7.2f %5d %9d\n",values[0],(double)(values[2])/P,values[1],values[2]);
  }
  return values[1];
}

static VW min_max_VW (int P, VW *l, int Output)
{ int   i;
  VW    values[3];

  if (P > 0)
  { values[0] = INT_MAX;
    values[1] = values[2] = 0;
    for (i=0; i<P; i++)
    { if (Output > 2)
	printf ("%.2f ",(double)(l[i]));
      values[2] += l[i];
      values[0] = MIN(values[0],l[i]);
      values[1] = MAX(values[1],l[i]);
    }
    if (Output > 0)
      printf("%9.2f %7.2f %5.2f %9.2f\n",(double)(values[0]),(double)(values[2])/P,(double)(values[1]),(double)(values[2]));
  }
  return values[1];
}

int part_info (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part, int Output)
{ int	i, j, is_internal, *size, *internal, *neig, *cut, *cut_w, *channel, 
        total_cut_size=0, total_cut_size_w=0, max_size,
	*locked, *queue, *cc, first_unlocked;

  if (Output <= 0)
    return 0;
  puts("---------- Partition Information (min/ave/max/tot) ----------------");

  CALLOC ("PART_INFO",size,int,p);
  CALLOC ("PART_INFO",internal,int,p);
  CALLOC ("PART_INFO",neig,int,p);
  CALLOC ("PART_INFO",cut,int,p);
  CALLOC ("PART_INFO",channel,int,p*(p-1)/2);
  CALLOC ("PART_INFO",cut_w,int,p);

  for (i=0; i<n; i++)
  { if (part[i]<0 || part[i]>=p)
    { fprintf(stderr, "PART_INFO ERROR...wrong part number of vertex %d: %d\n",i,part[i]);
      return 1;
    }
    size[part[i]]++;
    is_internal = 1;
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
    { if (part[i] != part[edge[j]])
      { is_internal = 0;
	cut[part[i]] ++;
	cut_w[part[i]] += (edge_w?edge_w[j]:1);
	if (part[i] > part[edge[j]])
	  channel[(part[i])*((part[i])-1)/2+part[edge[j]]] = 1;
      }
    }
    internal[part[i]] += is_internal;
  }
   
  for (i=0; i<p; i++)
  { total_cut_size += cut[i];
    total_cut_size_w += cut_w[i]; 
    for (j=0; j<i; j++)
      if (channel[i*(i-1)/2+j])
      { neig[i]++;
        neig[j]++;
      }
  }

  /* Components */
  CALLOC ("PART_INFO",locked,int,n);
  MALLOC ("PART_INFO",queue,int,n);
  CALLOC ("PART_INFO",cc,int,p);
  i = first_unlocked = 0;
  while (i<n)
  { int k, size=1, queue_head=0, queue_tail=1, vertex;
    while (locked[first_unlocked])
      first_unlocked++;
    queue[0] = first_unlocked;
    locked[first_unlocked] = 1;
    cc[part[first_unlocked]]++;
    while (queue_head < queue_tail)
    { vertex = queue[queue_head++];
      for (k=edge_p[vertex]; k<edge_p[vertex+1]; k++)
        if (part[edge[k]]==part[vertex] && !(locked[edge[k]]))
        { size++;
	  locked[edge[k]] = 1;
          queue[queue_tail++] = edge[k]; 
        }
    }
    i += size;
  }
  printf ("Components          :"); min_max (p, cc, Output);
  FREE(locked,int,n);
  FREE(queue,int,n);
  FREE(cc,int,p);

  printf ("VERTEX-based:\n");
  printf (" Size               :"); max_size = min_max (p, size, Output);
  printf (" Internal           :"); min_max (p, internal, Output);
  printf (" Balance            : %.5f\n", (double)max_size*p/n);
  FREE(size,int,p); 
  FREE(internal,int,p); 
  if (vertex_w)
  { VW *size_w, *internal_w, max_size_w, tot_weight;
    TOT_WEIGHT(tot_weight,n,vertex_w);
    CALLOC ("PART_INFO",size_w,VW,p);
    CALLOC ("PART_INFO",internal_w,VW,p);
    for (i=0; i<n; i++)
    { size_w[part[i]] += (vertex_w[i]);
      for (j=edge_p[i]; j<edge_p[i+1] && part[edge[j]]==part[i]; j++);
      if (j == edge_p[i+1])
	internal_w[part[i]] += (vertex_w[i]);
    }
    printf (" Size w.            :"); max_size_w = min_max_VW (p,size_w,Output);
    printf (" Internal w.        :"); min_max_VW (p,internal_w,Output);
    printf (" Balance w.         : %.3f\n", (double)max_size_w*p/tot_weight);
    FREE(size_w,VW,p); 
    FREE(internal_w,VW,p); 
  }

  printf ("EDGE-based:\n");
  printf (" Part Deg.          :"); min_max (p,neig,Output);
  printf (" External           :"); min_max (p,cut,Output);
  printf (" Total cut size     : %d\n", total_cut_size/2);
  if (edge_w)
  { printf (" External w.        :"); min_max (p,cut_w,Output);
    printf (" Total w. cut size  : %d\n", total_cut_size_w/2);
  }
  FREE(neig,int,p);
  FREE(cut,int,p); 
  FREE(channel,int,p*(p-1)/2);
  FREE(cut_w,int,p); 
  puts("-------------------------------------------------------------------");
  return 0;
}



int graph_reduce (int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w,
	int red_n, VW *red_vertex_w, float *red_x, float *red_y, float *red_z,
	int *red_edge_p, int *red_edge, int *red_edge_w,
	int *matching, int *red_to, int *superedge)
{ int	i, j, new_vertex=0, new_edge=0, neigh_to, match,
	edge_p_i1, edge_p_m1;

  if (vertex_w)
  { for (i=0; i<n; i++)
      if (matching[i] > i)
      { red_vertex_w[new_vertex] = vertex_w[i]+vertex_w[matching[i]];
        red_to[i] = red_to[matching[i]] = new_vertex++; 
      }
      else if (matching[i] == i)
      { red_vertex_w[new_vertex] = vertex_w[i];
        red_to[i] = new_vertex++;
      }
  }
  else
    for (i=0; i<n; i++)
      if (matching[i] > i)
      { red_vertex_w[new_vertex] = 2;
        red_to[i] = red_to[matching[i]] = new_vertex++; 
      }
      else if (matching[i] == i)
      { red_vertex_w[new_vertex] = 1;
        red_to[i] = new_vertex++; 
      }
  red_edge_p[0] = 0;

  for (i=0; i<n; i++)
    if ((match=matching[i]) > i)
    { edge_p_i1 = edge_p[i+1];
      edge_p_m1 = edge_p[match+1];
      for (j=edge_p[i]; j<edge_p_i1; j++)
	superedge[red_to[edge[j]]] += (edge_w?edge_w[j]:1);
      for (j=edge_p[match]; j<edge_p_m1; j++)
        superedge[red_to[edge[j]]] += (edge_w?edge_w[j]:1);
      superedge[red_to[i]] = 0;
      for (j=edge_p[i]; j<edge_p_i1; j++)
	if (superedge[neigh_to=red_to[edge[j]]])
	{ red_edge[new_edge] = neigh_to;
	  red_edge_w[new_edge++] = superedge[neigh_to];
	  superedge[neigh_to] = 0;
	}
      for (j=edge_p[match]; j<edge_p_m1; j++)
	if (superedge[neigh_to=red_to[edge[j]]])
	{ red_edge[new_edge] = neigh_to;
	  red_edge_w[new_edge++] = superedge[neigh_to];
	  superedge[neigh_to] = 0;
	}
      red_edge_p[red_to[i]+1] = new_edge;
    }
    else if (match == i)
    { edge_p_i1 = edge_p[i+1];
      for (j=edge_p[i]; j<edge_p_i1; j++)
	superedge[red_to[edge[j]]] += (edge_w?edge_w[j]:1);
      for (j=edge_p[i]; j<edge_p_i1; j++)
        if (superedge[neigh_to=red_to[edge[j]]])
	{ red_edge[new_edge] = neigh_to;
	  red_edge_w[new_edge++] = superedge[neigh_to];
	  superedge[neigh_to] = 0;
	}
      red_edge_p[red_to[i]+1] = new_edge;
    }

  if (x)
  { if (vertex_w)
    { for (i=0; i<n; i++)
        if ((match=matching[i]) >= i)
        { red_x[red_to[i]] = (vertex_w[i]*x[i]+vertex_w[match]*x[match])/(vertex_w[i]+vertex_w[match]);
	  if (y)
	    red_y[red_to[i]] = (vertex_w[i]*y[i]+vertex_w[match]*y[match])/(vertex_w[i]+vertex_w[match]);
	  if (z)
	    red_z[red_to[i]] = (vertex_w[i]*z[i]+vertex_w[match]*z[match])/(vertex_w[i]+vertex_w[match]);
        }
    }
    else
    { for (i=0; i<n; i++)
	if ((match=matching[i]) >= i)
	{ red_x[red_to[i]] = (x[i]+x[match])/2;
	  if (y)
	    red_y[red_to[i]] = (y[i]+y[match])/2;
	  if (z)
	    red_z[red_to[i]] = (z[i]+z[match])/2;
	}
    }
  }
  return 0;
}
