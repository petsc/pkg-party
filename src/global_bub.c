/**********************************************************\
*  PARTY PARTITIONING LIBRARY            global_bub.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

static int distance (int seed, VW *vertex_w, int *edge_p, 
	int *edge, int *edge_w, int *part, int *dist, int *center, 
	BUCKETS *B, BUCKET_ELE *bucket_ele, int mod)
{ int 		j, neigh, sum=0, vertex, part_seed=part[seed];
  BUCKET_ELE 	*element;

  center[seed] = seed;
  dist[seed] = 0;
  bucket_in_mod(B,&(bucket_ele[seed]),0,mod);
  while ((element=bucket_out_mod(B,mod))) 
  { vertex = (int)(element->element);
    for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
    { neigh = edge[j];
      if (part[neigh]==part_seed)
      { if (center[neigh] != seed) 
	{ center[neigh] = seed;
	  dist[neigh] = dist[vertex]+(edge_w?edge_w[j]:1);
	  bucket_in_mod(B,&(bucket_ele[neigh]),-dist[neigh],mod);
	  sum += (vertex_w?(dist[neigh]*vertex_w[neigh]):dist[neigh]);
        }
        else if (dist[neigh]>dist[vertex]+(edge_w?edge_w[j]:1))
        { if (buckets(bucket_ele[neigh]))
	  { sum -= (vertex_w?(dist[neigh]*vertex_w[neigh]):dist[neigh]);
	    dist[neigh] = dist[vertex]+(edge_w?edge_w[j]:1);
	    bucket_new_key_mod(&(bucket_ele[neigh]),-dist[neigh],mod);
          }
	  else
	  { dist[neigh] = dist[vertex]+(edge_w?edge_w[j]:1);
	    bucket_in_mod(B,&(bucket_ele[neigh]),-dist[neigh],mod);
          }
          sum += (vertex_w?(dist[neigh]*vertex_w[neigh]):dist[neigh]);
  } } } }
  return sum;
}

static int makepart (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int p, int *part, int *seed, int *part_queue, 
	VW *part_weight, int *dist, BUCKETS **B, BUCKET_ELE *bucket_ele,
	int mod)
{ int i, j, vertex=-1, considered=p, part_head=0, part_tail=0, done, change;
  BUCKET_ELE	*element;

  for (i=0; i<n; i++)
    part[i] = -1;
  for (i=0; i<p; i++) 
  { bucket_clear_mod(B[i],mod);
    part[seed[i]] = i;
    dist[seed[i]] = 0;
    bucket_in_mod(B[i], &(bucket_ele[seed[i]]), 0,mod);
    part_weight[i] = vertex_w ? vertex_w[seed[i]] : 1;
    part_head = (part_head+p-1)%p;
    part_queue[part_head] = i;
    for (j=part_head; (j+1)%p!=part_tail && part_weight[part_queue[j]]>part_weight[part_queue[(j+1)%p]]; j=(j+1)%p)
    { change = part_queue[j];
      part_queue[j] = part_queue[(j+1)%p];
      part_queue[(j+1)%p] = change;
  } }

  while (considered < n)
  { done = 0;
    i = part_queue[part_head];
    while (!done && (element=bucket_out_mod(B[i],mod)))
    { vertex = (int)(element->element);
      for (j=edge_p[vertex]; !done && j<edge_p[vertex+1]; j++) 
	if (part[edge[j]] == -1)	
	{ considered++;
	  done = 1;
	  part[edge[j]] = i;
	  dist[edge[j]] = dist[vertex] + (edge_w?edge_w[j]:1);
	  bucket_in_mod(B[i], &(bucket_ele[edge[j]]), -dist[edge[j]],mod);
	  part_weight[i] += vertex_w ? vertex_w[edge[j]] : 1;
	  for (j=part_head; (j+1)%p!=part_tail && part_weight[part_queue[j]]>part_weight[part_queue[(j+1)%p]]; j=(j+1)%p)
          { change = part_queue[j];
	    part_queue[j] = part_queue[(j+1)%p];
	    part_queue[(j+1)%p] = change;
	  }
          if (Xpart>=3 && considered>=p)
	  { int i;
	    for (i=0; i<p; i++)
	      part[seed[i]] = p;
	    sprintf (xpart_c, "bub part %d",i);
	    XPART(3,"GLOBAL_BUBBLE",part);
	    for (i=0; i<p; i++)
	      part[seed[i]] = i;
	  }
        }
	else if (part[edge[j]]==i && buckets(bucket_ele[edge[j]]) && dist[edge[j]]>dist[vertex]+(edge_w?edge_w[j]:1))
	{ dist[edge[j]] = dist[vertex]+(edge_w?edge_w[j]:1);
	  bucket_new_key_mod(&(bucket_ele[edge[j]]),-dist[edge[j]],mod);
    }   }
    if (done)
      bucket_in_mod (B[i],&(bucket_ele[vertex]),-dist[vertex],mod);
    else
    { part_head = (part_head+1)%p;
      if (part_head == part_tail)
      { /* Search for another connected component */
        int min_part = 0;
	for (i=1; i<p; i++)
	  if (part_weight[i] < part_weight[min_part])
	    min_part = i;
	for (i=0; i<n && part[i]!=-1; i++) /*has to be empty*/;
	considered++;
	part[i] = part_queue[0] = min_part;
	dist[i] = 0;
	bucket_in_mod(B[min_part], &(bucket_ele[i]), -dist[i], mod);
        part_weight[min_part] = vertex_w ? vertex_w[i] : 1;
	part_head = (part_head+p-1)%p;
	part_queue[part_head] = min_part;
  } } }
  return 0;
}

int global_bub (int n, VW *vertex_w, int *edge_p, int *edge, 
	int *edge_w, int p, int *part, int *seed, int Output)
{ int	i, j, change=1, iteration=0, seed_set=0, *seed_best, cut=0,
	cut_best=INT_MAX, loop=0, not_improved=0, *dist, *dist_sum, *center, 
	*part_queue, max_edge_w=1;
  VW	*part_weight;
  BUCKETS	**B;
  BUCKET_ELE	*bucket_ele;

  CALLOC("GLOBAL_BUBBLE",seed_best,int,p);
  CALLOC("GLOBAL_BUBBLE",dist,int,n);
  CALLOC("GLOBAL_BUBBLE",dist_sum,int,n);
  CALLOC("GLOBAL_BUBBLE",center,int,n);
  CALLOC("GLOBAL_BUBBLE",bucket_ele,BUCKET_ELE,n);
  for (i=0; i<n; i++)
    bucket_ele[i].element = (void*)i;
  if (edge_w)
    for (i=0; i<edge_p[n]; i++)
      max_edge_w = MAX(max_edge_w,edge_w[i]);
  CALLOC("GLOBAL_BUBBLE",B,BUCKETS*,p);
  for (i=0; i<p; i++)
    if (bucket_cal(&(B[i]),-max_edge_w, 0))
      FAILED ("GLOBAL_BUBBLE", "bucket_cal");
  CALLOC("GLOBAL_BUBBLE",part_queue,int,p);
  CALLOC("GLOBAL_BUBBLE",part_weight,VW,p);

  /* Initial seeds for Partitions */
  if (!seed)
  { int	k, vertex, next_free_vertex, *queue, front=0, end=0;

    seed_set = 1;
    CALLOC ("GLOBAL_BUBBLE",seed,int,p);
    CALLOC ("GLOBAL_BUBBLE",queue,int,n);
    for (i=0; i<n; i++)
      part[i] = -1;

    for (i=0; i<p; i++)
    { for (j=0; j<i; j++)
      { queue[j] = seed[j];
	part[seed[j]] = i;
      }
      front = 0;
      end = i;
      next_free_vertex = 0;
      while (end < n)
      { if (front == end)
	{ while(part[next_free_vertex]==i)
	    next_free_vertex++;
          part[next_free_vertex] = i;
	  queue[end++] = next_free_vertex;
	}
        vertex = queue[front++];
	for (k=edge_p[vertex]; k<edge_p[vertex+1]; k++)
  	  if (part[edge[k]] <i)
	  { part[edge[k]] = i;
	    queue[end++] = edge[k];
      }   }
      seed[i] = queue[n-1];
    }
    FREE(queue,int,n);
  }

  while (change && !loop && not_improved<10)
  { change = 0;
    iteration++;

    if (makepart(n,vertex_w,edge_p,edge,edge_w,p,part,seed,part_queue,part_weight,dist,B,bucket_ele,max_edge_w+1))
      FAILED("GLOBAL_BUBBLE","makepart");
    cut = cut_size(n,edge_p,edge,edge_w,part);
    if (Output > 0)
    { printf("Seeds: ");
      for (i=0; i<p; i++)
        printf("%d ",seed[i]);
      printf("\nBUB Iteration %d not_improved:%d with cutsize %d\n",iteration,not_improved,cut);
    }
    if (cut < cut_best)
    { not_improved = 0;
      cut_best = cut;
      for (i=0; i<p; i++)
	seed_best[i] = seed[i];
    }
    else
      not_improved++;

    for (i=0; i<p; i++)
      bucket_clear_mod(B[i],max_edge_w+1);
    for (i=0; i<n; i++)
      dist_sum[i] = dist[i] = center[i] = INT_MAX;
    for (i=0; i<p; i++)
    { int improved=1;
      dist_sum[seed[i]] = distance(seed[i],vertex_w,edge_p,edge,edge_w,part,dist,center,B[0],bucket_ele,max_edge_w+1);
      while (improved)
      { improved = 0;
        for (j=edge_p[seed[i]]; !improved && j<edge_p[seed[i]+1]; j++)
	  if (part[edge[j]] == i  &&  dist_sum[edge[j]] == INT_MAX)
	  { dist_sum[edge[j]] = distance(edge[j],vertex_w,edge_p,edge,edge_w,part,dist,center,B[0],bucket_ele,max_edge_w+1);
	    if (dist_sum[edge[j]] < dist_sum[seed[i]])
	    { improved = change = 1;
	      seed[i] = edge[j];
    } }   } }
    loop = 1;
    for (i=0; i<p && loop; i++)
      if (seed[i] != seed_best[i])
        loop = 0;

    if (Xpart>=2)
    { for (i=0; i<p; i++)
        part[seed[i]] = p;
      sprintf (xpart_c, "bub iter %d",iteration);
      XPART(2,"GLOBAL_BUBBLE",part);
      for (i=0; i<p; i++)
        part[seed[i]] = i;
    }
  }

  if (cut_best < cut)
  { for (i=0; i<p; i++)
      seed[i] = seed_best[i];
    if (makepart(n,vertex_w,edge_p,edge,edge_w,p,part,seed,part_queue,part_weight,dist,B,bucket_ele,max_edge_w+1))
      FAILED("GLOBAL_BUBBLE","makepart");
  }

  FREE(seed_best,int,p);
  if (seed_set)
    FREE(seed,int,p);
  FREE(dist_sum,int,n);
  FREE(dist,int,n);
  FREE(center,int,n);
  FREE(bucket_ele,BUCKET_ELE,n);
  for (i=0; i<p; i++)
    bucket_free(B[i]);
  FREE(B,BUCKETS*,p);
  FREE(part_queue,int,p);
  FREE(part_weight,VW,p);
  return 0;
}
