/**********************************************************\
*  PARTY PARTITIONING LIBRARY            matching.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

/*
#define LAM_ORIG
*/

static int lam_match (int a, int b, int edgewab, VW *vertex_w, int* edge_p,
	int* edge, int* edge_w, int* matching, int* edge_p_check, int *red_n,
	int redl)
{ int	edge_p_check_a=edge_p_check[a], edge_p_check_b=edge_p_check[b],
	neigh, neigh_w, neigh_deg, edge_p_a1=edge_p[a+1], edge_p_b1=edge_p[b+1],
	deg_a=edge_p[a+1]-edge_p[a], deg_b=edge_p[b+1]-edge_p[b];

  while (matching[a]==a && matching[b]==b &&
	 (edge_p_check[a]<edge_p_a1 || edge_p_check[b]<edge_p_b1) &&
	 (*red_n)>redl )
  { if (edge_p_check[a]<edge_p_a1)
    { neigh = edge[edge_p_check[a]];
      neigh_w = (edge_w?edge_w[edge_p_check[a]]:1);
      neigh_deg = edge_p[neigh+1]-edge_p[neigh];
      (edge_p_check[a])++;
#ifdef LAM_ORIG
      if (neigh_w>edgewab)
#else
      if ((deg_b>1&&neigh_deg>1&&(neigh_w>edgewab||(neigh_w==edgewab&&neigh_deg<deg_b))) ||
	  (deg_b==1&&neigh_deg==1&&neigh_w>edgewab) ||
	  (deg_b==1&&neigh_deg>1&&neigh_w>2*edgewab) ||
	  (deg_b>1&&neigh_deg==1&&2*neigh_w>=edgewab) )
#endif
        lam_match(a,neigh,neigh_w,vertex_w,edge_p,edge,edge_w,matching,edge_p_check,red_n,redl);
    }
    if (matching[b]==b && edge_p_check[b]<edge_p_b1)
    { neigh = edge[edge_p_check[b]];
      neigh_w = (edge_w?edge_w[edge_p_check[b]]:1);
      neigh_deg = edge_p[neigh+1]-edge_p[neigh];
      (edge_p_check[b])++;
#ifdef LAM_ORIG
      if (neigh_w>edgewab)
#else
      if ((deg_a>1&&neigh_deg>1&&(neigh_w>edgewab||(neigh_w==edgewab&&neigh_deg<deg_a))) ||
	  (deg_a==1&&neigh_deg==1&&neigh_w>edgewab) ||
	  (deg_a==1&&neigh_deg>1&&neigh_w>2*edgewab) ||
	  (deg_a>1&&neigh_deg==1&&2*neigh_w>=edgewab) )
#endif
	lam_match(b,neigh,neigh_w,vertex_w,edge_p,edge,edge_w,matching,edge_p_check,red_n,redl);
    }
  }

  if (matching[a]==a && matching[b]==b && (*red_n)>redl)
  { matching[a] = b;
    matching[b] = a;
    (*red_n)--;
  }
  else if (matching[a]==a && matching[b]!=b)
    edge_p_check[a] = edge_p_check_a;
  else if (matching[a]!=a && matching[b]==b)
    edge_p_check[b] = edge_p_check_b;

  return 0;
}

int matching_lam (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int redl, int *matching, int *red_n)
{ int	i, j, *edge_p_check;

  for (i=0; i<n; i++)  
    matching[i] = i;
  (*red_n) = n;
  MALLOC("MATCHING", edge_p_check, int, n+1);
  memcpy(edge_p_check,edge_p,(n+1)*sizeof(int));

  for (i=0; i<n; i++)
    for (j=edge_p[i]; matching[i]==i && j<edge_p[i+1]; j++)
      if (matching[edge[j]] == edge[j])
        lam_match(i,edge[j],(edge_w?edge_w[j]:1),vertex_w,edge_p,edge,edge_w,matching,edge_p_check,red_n,redl);
  FREE(edge_p_check, int, n+1);
  return 0;
}


int matching_opt_w3 (int n, VW *vertex_w, int *edge_p, int *edge, int* edge_w,
	int *matching, int* red_n, int redl)
{ int	i, j, k, *stack_free, free_p=0, matching_weight,
  	gain_2, best_2=-1, best_2_w, gain_3, best_near=-1, best_middle=-1,
	best_distant=-1, match_edgew_ab, match_edgew_cd;

  MALLOC("LOWM23",stack_free,int,n);

  for (i=0; i<n; i++) 
    if (matching[i] == i)
      stack_free[free_p++] = i;

  while (free_p && (*red_n)>redl)
  { i = stack_free[--free_p];
    gain_2 = gain_3 = -1;
    for (j=edge_p[i]; matching[i]==i && j<edge_p[i+1]; j++)
    { if (matching[edge[j]]==edge[j])
      { matching[i] = edge[j];
	matching[edge[j]] = i;
	j=edge_p[i+1];
	(*red_n)--;
      }
      else 
      { for (k=edge_p[edge[j]]; matching[edge[j]]!=edge[k]; k++) ;
	matching_weight = edge_w?edge_w[k]:1;
	if ((edge_w?edge_w[j]:1)-matching_weight > gain_2)
        { gain_2 = (edge_w?edge_w[j]:1)-matching_weight;
          best_2 = edge[j];
          best_2_w = edge_w?edge_w[j]:1;
        }
	  
        for (k=edge_p[matching[edge[j]]]; k<edge_p[matching[edge[j]]+1]; k++)
          if (matching[edge[k]]==edge[k] && edge[k]!=i)
            if((edge_w?edge_w[j]:1)-matching_weight+(edge_w?edge_w[k]:1)>gain_3)
	    { best_near = edge[j];
	      best_middle = matching[edge[j]];
	      best_distant = edge[k];
	      gain_3 = (edge_w?edge_w[j]:1)-matching_weight+(edge_w?edge_w[k]:1);
              match_edgew_ab=edge_w?edge_w[j]:1;
	      match_edgew_cd=edge_w?edge_w[k]:1; 
    } }     }

    if (matching[i]==i)
    { if (gain_3 >= 0)
      { matching[i] = best_near;
	matching[best_near] = i;
	matching[best_middle] = best_distant;
	matching[best_distant] = best_middle;
	(*red_n)--;
      }
      else if (gain_2>0)
      { matching[i] = best_2;
        stack_free[free_p++] = matching[best_2];
        matching[matching[best_2]] = matching[best_2];
        matching[best_2] = i;
  } } }
  FREE(stack_free,int,n);
  return 0;
}

