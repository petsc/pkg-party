/**********************************************************\
*  PARTY PARTITIONING LIBRARY            io.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

static int graph_load_grid (
	int *n, float **x, float **y, int **edge_p, int **edge)
{ int 	i, j, e, size=100;

  (*n) = size*size;
  e = 4*size*(size-1);
  MALLOC ("GRAPH_LOAD_GRID",*edge_p,int,(*n)+1);
  MALLOC ("GRAPH_LOAD_GRID",*edge,int,e);
  MALLOC ("GRAPH_LOAD_GRID",*x,float,*n);
  MALLOC ("GRAPH_LOAD_GRID",*y,float,*n);

  e = 0;
  (*edge_p)[0] = 0;
  for (i=0; i<size; i++)
    for (j=0; j<size; j++)
    { if (i>0)
        (*edge)[e++] = size*(i-1)+j;
      if (j>0)
	(*edge)[e++] = size*i+j-1;
      if (j<size-1)
	(*edge)[e++] = size*i+j+1;
      if (i<size-1)
	(*edge)[e++] = size*(i+1)+j;
      (*edge_p)[size*i+j+1] = e;
      (*x)[size*i+j] = i;
      (*y)[size*i+j] = j;
    }
  return 0;
}

int graph_load (
	int *n, VW **vertex_w, float **x, float **y, float **z,
	int **edge_p, int **edge, int **edge_w, char *graphfile, char *xyzfile)
{ FILE  *f, *fxyz;                          
  int   i, e, Edge=0, neig, code=0, with_vw=0, with_ew=0;
  char  *s, string[100000];

  if (!strcmp(graphfile,"Grid100x100"))
  { if (graph_load_grid (n,x,y,edge_p,edge))
      FAILED ("GRAPH_LOAD", "graph_load_grid");
    return 0;
  }

  if (!(f = fopen( graphfile, "r" )))
  { fprintf(stderr, "GRAPH_LOAD ERROR...not able to read file %s!\n",graphfile);
    return 1;
  }

/* COMMENT LINES */
  if (!(fgets (string, 100000, f)))
  { fprintf(stderr, "GRAPH_LOAD ERROR...failed to read first line from file %s!\n",graphfile);
    return 1;
  }
  while (string[0] == '%'  ||  string[0] == '#')
    if (!(fgets (string, 100000, f)))
    { fprintf(stderr, "GRAPH_LOAD ERROR...failed to read graph from file %s!\n",graphfile);
      return 1;
    }

/* HEAD LINE */
  *n = atoi (strtok(string," \n"));
  if ((s=strtok(0," \n")))
    e = 2 * atoi (s);
  else
    ERROR ("GRAPH_LOAD", "missing number of edges in head line");
  if ((s=strtok(0," \n")))
    code = atoi (s);

  MALLOC ("GRAPH_LOAD",*edge_p,int,(*n)+1);
  MALLOC ("GRAPH_LOAD",*edge,int,e);
  (*edge_p)[0] = 0;
  if ((code/100)%10 == 1)
  { fprintf(stderr, "GRAPH_LOAD ERROR...the option 1xx of the code in the first line is not accepted!\n");
    return 1;
  }
  if ((code/10)%10 == 1)
  { MALLOC ("GRAPH_LOAD",*vertex_w,VW,*n);
    with_vw = 1;
  }
  if (code%10 == 1)
  { MALLOC ("GRAPH_LOAD",*edge_w,int,e);
    with_ew = 1;
  }

/* n VERTICES LINES */
  for (i=0; i<*n; i++)
  { if (!(fgets (string, 100000, f)))
    { fprintf(stderr, "GRAPH_LOAD ERROR...failed to read vertex %d from file %s!\n",i,graphfile);
      return 1;
    }
    s = strtok(string," \n");

    if (with_vw)
    { (*vertex_w)[i] = (VW)atof(s);
      s = strtok(NULL," \n");
    }

    while (s)
    { neig = atoi(s);
      s = strtok(NULL," \n");
      if (neig<=0 || neig>*n)
      { fprintf(stderr, "GRAPH_LOAD ERROR...neighbor %d of vertex %d is out of range [%d,%d]!\n",neig,i+1,1,*n);
        return 1;
      }
      if (Edge >= e)
      { fprintf(stderr, "GRAPH_LOAD ERROR...found at least %d edges, but only %d mentioned in header\n",Edge,e);
	return 1;
      }
      if (with_ew)
      { (*edge_w)[Edge] = atoi(s);
        s = strtok(NULL," \n");
      }
      (*edge)[Edge++] = neig-1;
    }
    (*edge_p)[i+1] = Edge;
  }
  if (fscanf(f, "%d", &i) != EOF)
  { fprintf(stderr, "GRAPH_LOAD ERROR... file %s is too long!\n", graphfile);
    return 1;
  }
  if (fclose(f))
    FAILED ("GRAPH_LOAD", "fclose");
  if (Edge != e)
  { fprintf(stderr, "GRAPH_LOAD ERROR...wrong number of %d edges in file...I counted %d directed edges, it should be %d edges in the first line of the file!\n",e/2,Edge,(Edge)/2);
    return 1;
  }
   
  if (xyzfile)
  { if (!(fxyz=fopen(xyzfile,"r")))
    { fprintf(stderr, "GRAPH_LOAD ERROR...not able to read file %s!\n",xyzfile);
      return 1;
    }
    if (!(fgets (string, 100000, fxyz)))
    { fprintf(stderr, "GRAPH_LOAD ERROR...failed to read first line from file %s!\n",xyzfile);
      return 1;
    }
    if ((s=strtok(string," \n")))
    { MALLOC ("GRAPH_LOAD",*x,float,*n); 
      (*x)[0] = atof (s);
      if ((s=strtok(0," \n")))
      { MALLOC ("GRAPH_LOAD",*y,float,*n);
	(*y)[0] = atof (s);
        if ((s=strtok(0," \n")))
	{ MALLOC ("GRAPH_LOAD",*z,float,*n);
	  (*z)[0] = atof(s); 
        }
      }
    }

    for (i=1; i<*n; i++)
    { if (!(fgets (string, 100000, fxyz)))
      { fprintf(stderr, "GRAPH_LOAD ERROR...failed to read line %d from file %s!\n",i,xyzfile);
        return 1;
      }
      if (*x)
      { if ((s=strtok(string," \n"))) 
	  (*x)[i] = atof (s);
        else
        { fprintf(stderr, "GRAPH_LOAD ERROR...no x coordinate of vertex %d in file %s!\n",i,xyzfile);
  	  return 1;
        }
      }
      if (*y)
      { if ((s=strtok(0," \n")))
	  (*y)[i] = atof (s);
        else
        { fprintf(stderr, "GRAPH_LOAD ERROR...no y coordinate of vertex %d in file %s!\n",i,xyzfile);
	  return 1;
        }
      }
      if (*z)
      { if ((s=strtok(0," \n")))
	  (*z)[i] = atof(s);
        else
	{ fprintf(stderr, "GRAPH_LOAD ERROR...no z coordinate of vertex %d in file %s!\n",i,xyzfile);
	  return 1;
	}
      }
    }
    if (fclose(fxyz))
      FAILED ("GRAPH_LOAD", "fclose");
  }
  return 0;
}


int graph_save (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w, char *graphfile, char *xyzfile)
{ FILE 	*f, *fxyz;                          
  int	i, j;

  if (!(f = fopen( graphfile, "w")))
  { fprintf(stderr,"GRAPH_SAVE ERROR...not able to write file %s!\n",graphfile);
    return 1;
  }
  fprintf(f,"%d %d",n,edge_p[n]/2);
  if (vertex_w || edge_w)
  { fprintf(f," 0");
    if (vertex_w)
      fprintf(f,"1");
    else
      fprintf(f,"0");
    if (edge_w)
      fprintf(f,"1");
    else
      fprintf(f,"0");
  }
  fprintf(f,"\n");
  for (i=0; i<n; i++)
  { if (vertex_w)
      fprintf(f,"%d ",(int)vertex_w[i]);
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
    { fprintf(f,"%d",edge[j]+1);
      if (edge_w)
        fprintf(f," %d",edge_w[j]);
      if (j<edge_p[i+1]-1)
        fprintf(f," ");
    }
    fprintf(f,"\n");
  }
  if (fclose(f))
    FAILED ("GRAPH_SAVE", "fclose");

  if (x)
  { if (!(fxyz=fopen(xyzfile,"w")))
    { fprintf(stderr,"GRAPH_SAVE ERROR...not able to write file %s!\n",xyzfile);
      return 1;
    }
    for (i=0; i<n; i++)
    { fprintf(fxyz,"%f",x[i]);
      if (y)
      { fprintf(fxyz," %f",y[i]);
	if (z)
	  fprintf(fxyz," %f",z[i]);
      }
      fprintf(fxyz,"\n");
    }
    if (fclose(fxyz))
      FAILED ("GRAPH_SAVE", "fclose");
  }
  return 0;
}

int graph_free (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w)
{ FREE(x,float,n);
  FREE(y,float,n);
  FREE(z,float,n);
  FREE(vertex_w,VW,n);
  FREE(edge_w,int,edge_p[n]);
  FREE(edge,int,edge_p[n]);
  FREE(edge_p,int,n+1);
  return 0;
}

int graph_print (
	int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w)
{ int	i;

  printf("n: %d\n",n);
  if (vertex_w)
    for (i=0; i<n; i++)
      printf("vertex_w[%d]: %f\n",i,(double)(vertex_w[i]));
  if (x)
    for (i=0; i<n; i++)
      printf("x[%d]: %f\n",i,x[i]);
  if (y)
    for (i=0; i<n; i++)
      printf("y[%d]: %f\n",i,y[i]);
  if (z)
    for (i=0; i<n; i++)
      printf("z[%d]: %f\n",i,z[i]);
  for (i=0; i<=n; i++)
    printf("edge_p[%d]: %d\n",i,edge_p[i]);
  for (i=0; i<edge_p[n]; i++)
    printf("edge[%d]: %d\n",i,edge[i]);
  if (edge_w)
    for (i=0; i<edge_p[n]; i++)
      printf("edge_w[%d]: %d\n",i,edge_w[i]);
  return 0;
}


int part_save (
	int n, int *part, char *partfile)
{ FILE  *f;
  int   i;

  if (!(f = fopen( partfile, "w" )))
    fprintf(stderr, "PART_SAVE ERROR...not able to write file '%s'\n",partfile);
  else
  { for (i=0; i<n; i++)
      if (fprintf (f,"%d\n", part[i]) == EOF)
        FAILED ("PART_SAVE", "fprintf");
    if (fclose(f))
      FAILED ("PART_SAVE", "fclose");
  }
  return 0;
}
