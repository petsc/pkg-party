/**********************************************************\
*  PARTY PARTITIONING LIBRARY            party.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"
#undef SET_VERTEX_WEIGHTS_TO_DEGREE

static long	t=0, t_init=0,
		t_load=0, t_part=0, t_rest=0;

static void times_output (int Times)
{ long t_all=t_load+t_part+t_rest;
  printf("TIME                : %.2f sec", (double)t_all/1000);
  if (t_all/60000)
    printf ("  (%.2f min)", (double)t_all/60000);
  printf ("\n");
  if (Times > 1) 
  { printf("  Load/Check        : %.2f\n",(double)t_load/1000);
    printf("  Part              : %.2f\n",(double)t_part/1000);
    if (Times > 2)
      party_lib_times_output (Times-2);
    printf("  Rest              : %.2f\n",(double)t_rest/1000);
  }
}

int main (int argc, char **argv)
{ int         	i, cutsize, redl=0, p=2, Output=1, Times=2, rec=1,
		n=0, *edge_p=NULL, *edge=NULL, *edge_w=NULL, *part;
  char        	graphfile[100]="Grid100x100", *xyzfile=NULL,
		global[100]="def", local[100]="hs",
		redm[100]="lam", redo[100]="w3", partfile[100]="";
  VW	 	*vertex_w=NULL;
  float		*x=NULL, *y=NULL, *z=NULL;

  Xpart = 0;
  id = NULL;

/* Start of the time*/
  INIT_TIME();
  party_lib_times_start();

/* check parameter */
  if (argc == 1)
  { puts("party [-flag value] [] [] ...");
    puts("-f    graphfile (Grid100x100)        -g global method:{(def),...}");
    puts("-xyz  coord. file (Grid100x100.xyz)  -l local method: {(hs),kl,no}");
    puts("-p    # of parts (2)                 -r recursive?: 0/(1)");
    puts("-redl reduction level (0)            -s save in partfile ()");
    puts("-redm reduction method (lam)         -o output: 0..4 (1)");
    puts("-redo reduction optimize (w3)        -t times output: 0..4 (2)");
    puts("\nExample with default values in brackets ():");
  }
  i = 0;
  while (++i<argc)
  { if     (!strcmp(argv[i],"-f")   && i+1<argc) strcpy (graphfile, argv[++i]);
    else if(!strcmp(argv[i],"-xyz") && i+1<argc) xyzfile = argv[++i];
    else if(!strcmp(argv[i],"-p")   && i+1<argc) p = atoi(argv[++i]);
    else if(!strcmp(argv[i],"-redl")&& i+1<argc) redl = atoi(argv[++i]);
    else if(!strcmp(argv[i],"-redm")&& i+1<argc) strcpy (redm, argv[++i]);
    else if(!strcmp(argv[i],"-redo")&& i+1<argc) strcpy (redo, argv[++i]);
    else if(!strcmp(argv[i],"-g")   && i+1<argc) strcpy (global, argv[++i]);
    else if(!strcmp(argv[i],"-l")   && i+1<argc) strcpy (local, argv[++i]);
    else if(!strcmp(argv[i],"-r")   && i+1<argc) rec = atof(argv[++i]);
    else if(!strcmp(argv[i],"-s")   && i+1<argc) strcpy (partfile, argv[++i]);
    else if(!strcmp(argv[i],"-o")   && i+1<argc) Output = atoi(argv[++i]);
    else if(!strcmp(argv[i],"-t")   && i+1<argc) Times = atoi(argv[++i]);
    else if(!strcmp(argv[i],"-x")   && i+1<argc) Xpart = atoi(argv[++i]);
    else 
    { fprintf(stderr, "PARTY ERROR...option '%s' not legal or without value\n", argv[i]);
      return 1;
    }
  }
  if (Output > 0)
  { puts (VERSION);
    printf("Graph               : %s\n", graphfile);
    printf("p                   : %d\n", p);
    printf("redl/redm/redo      : %d / %s / %s\n", redl,redm,redo);
    printf("global/local/rec    : %s / %s / %d\n", global,local,rec);
  }
  ADD_NEW_TIME(t_rest);

/* load and check graph */
  if (graph_load(&n,&vertex_w,&x,&y,&z,&edge_p,&edge,&edge_w,graphfile,xyzfile))
    FAILED ("PARTY", "graph_load");
#ifdef SET_VERTEX_WEIGHTS_TO_DEGREE
  if (!vertex_w)
  { MALLOC ("PARTY",vertex_w,VW,n);
    for (i=0; i<n; i++)
      vertex_w[i] = edge_p[i+1]-edge_p[i];
  }
#endif
  if (graph_check_and_info (n,vertex_w,x,y,z,edge_p,edge,edge_w,Output))
    FAILED ("PARTY", "graph_check_and_info");
  ADD_NEW_TIME(t_load);

/* do partitioning */
  MALLOC ("PARTY",part,int,n);
  if (Xpart>0)
  { MALLOC ("PARTY",id,int,n);
    for (i=0; i<n; i++)
      id[i] = i;
  }
  if (party_lib(n,vertex_w,x,y,z,edge_p,edge,edge_w,p,part,&cutsize,redl,redm,redo,global,local,rec,Output-1))
    FAILED ("PARTY", "party_lib");
  ADD_NEW_TIME(t_part);  

/* check and output partition */
  if (part_check(n,vertex_w,p,part, Output-2))
    FAILED ("PARTY", "part_check"); 
  if (Output > 0)
    if (part_info (n,vertex_w,edge_p,edge,edge_w,p,part,Output))
      FAILED ("PARTY", "part_info");
  if (strcmp(partfile,""))
  { if (!strcmp(partfile,"def"))
    { if (strstr(graphfile, "/"))
	sprintf (partfile, "%s", strrchr(graphfile, '/')+1);
      else
	sprintf (partfile, "%s", graphfile);
      sprintf (partfile, "%s%s%s+%s%s%d", partfile, ".", global, local, ".", p);
    }
    if (part_save(n,part,partfile))
      FAILED ("PARTY", "part_save");
  }

  if (graph_free (n,vertex_w,x,y,z,edge_p,edge,edge_w))
    FAILED ("PARTY", "graph_free");
  FREE(part,int,n);

  FREE(id,int,n);
  XPART_EXIT();
  if (Output > 0)
    print_alloc_statistics();
  ADD_NEW_TIME(t_rest);
  END_TIME();
  if (Times > 0)
    times_output(Times);
  if (Output > 0)
    puts("========== PARTY End ==============================================");
  return 0;
}
