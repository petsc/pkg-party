/**********************************************************\
*  PARTY PARTITIONING LIBRARY            global.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

static int 	cut_best, solutions, solutions_best, branches;
static VW*	weight;
static int 	*status, **lb, *max_lb, *Stack, Stack_head;
static int 	N, *Edge_p, *Edge, *Edge_w, *Part, P, OUTPUT;
static VW	*Vertex_w;
static int branch (int cut)
{ int	i, j, k, new_cut, v, neig;
  VW	vertex_w_v;

  branches++;
  if (OUTPUT > 1)
  { printf("branch %4d: ",branches);
    for (i=0; i<P; i++)
      printf("%3.2f ",(double)(weight[i]));
    printf("   cut_best:%d cut:%3d\n",cut_best,cut);
  }

  if (Stack_head < 0) 
  { solutions++;
    if (cut < cut_best)
    { cut_best = cut;
      solutions_best = 1;
      for (i=0; i<N; i++)
	Part[i] = status[i];
      if (OUTPUT > 1)
        printf("GLOBAL_OPT New Cut of solution %4d: %5d\n",solutions,cut);
    }
    else if (cut == cut_best)
      solutions_best++;
    if (OUTPUT > 3)
      printf("GLOBAL_OPT Cut of solution %4d: %5d\n",solutions,cut);
  }
  else
  { v = Stack[Stack_head--];
    vertex_w_v = (Vertex_w?Vertex_w[v]:1);
    for (i=0; i<P; i++)
      if (weight[i] >= vertex_w_v)
      { status[v] = i;
	if ((new_cut=cut+max_lb[v]-lb[v][i])<cut_best || OUTPUT>2)
	{ for (j=Edge_p[v]; j<Edge_p[v+1]; j++)
	    if (status[neig=Edge[j]]<0)
            { new_cut += (Edge_w?Edge_w[j]:1);
	      lb[neig][i] += (Edge_w?Edge_w[j]:1);
	      if (lb[neig][i] > max_lb[neig])
	      { new_cut += (max_lb[neig] - lb[neig][i]);
		max_lb[neig] = lb[neig][i];
            } }
          if (new_cut<cut_best || OUTPUT>2)
  	  { weight[i] -= vertex_w_v;
	    if (branch (new_cut))
	      FAILED ("GLOBAL_OPT","branch");
	    weight[i] += vertex_w_v;
          }
	  for (j=Edge_p[v]; j<Edge_p[v+1]; j++)
	    if (status[neig=Edge[j]]<0)
	    { if (max_lb[neig] == lb[neig][i])
	      { lb[neig][i] -= (Edge_w?Edge_w[j]:1);
		max_lb[neig] = lb[neig][0];
	        for (k=1; k<P; k++)
	          if (lb[neig][k] > max_lb[neig])
	            max_lb[neig] = lb[neig][k];
              }
	      else
		lb[neig][i] -= (Edge_w?Edge_w[j]:1);
      } }   }
    status[v] = -1;
    Stack_head++;
  }
  return 0;
}
int global_opt (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part, int *locked, int Output)
{ int           i, j; 
  VW 		max_part_weight;

  if (n > 200)
    fprintf(stderr,"GLOBAL_OPT WARNING...graph with %d vertices is much too large for 'opt', it may take some lifetimes!\n",n);
  else if (n > 50)
    fprintf(stderr,"GLOBAL_OPT WARNING...graph with %d vertices is very large for 'opt', it may take some time!\n",n);

  N = n;
  Vertex_w = vertex_w;
  Edge_p = edge_p;
  Edge = edge;
  Edge_w = edge_w;
  Part = part;
  P = p;
  OUTPUT = Output;

  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);

  CALLOC ("GLOBAL_OPT",status,int,n);
  CALLOC ("GLOBAL_OPT",lb,int*,n);
  CALLOC ("GLOBAL_OPT",lb[0],int,n*p);
  CALLOC ("GLOBAL_OPT",max_lb,int,n);
  CALLOC ("GLOBAL_OPT",weight,VW,p);
  CALLOC ("GLOBAL_OPT",Stack,int,n);
  status[0] = -1;
  for (i=1; i<n; i++)
  { status[i] = -1;
    lb[i] = &(lb[0][i*p]);
  }
  for (i=0; i<p; i++)
    weight[i] = max_part_weight;
  Stack_head = -1;

  solutions = solutions_best = branches = 0;
  if (locked)
  { int neig, locked_vertices=0, cut=0;
    for (i=0; i<n; i++)
      if (locked[i]>=0  &&  locked[i]<p)
      { locked_vertices++;
	status[i] = locked[i];
	weight[locked[i]] -= (vertex_w?vertex_w[i]:1);
	cut += (max_lb[i] - lb[i][locked[i]]);
	for (j=Edge_p[i]; j<Edge_p[i+1]; j++)
          if (status[neig=Edge[j]]<0)
	  { lb[neig][locked[i]] += (Edge_w?Edge_w[j]:1);
	    cut += (Edge_w?Edge_w[j]:1);
	    if (lb[neig][locked[i]] > max_lb[neig])
	    { cut += (max_lb[neig] - lb[neig][locked[i]]);
	      max_lb[neig] = lb[neig][locked[i]];
      }   } }
      else
	Stack[++Stack_head] = i;
    if (Output > 0)
      printf("GLOBAL_OPT...%d locked and %d variable vertices.\n",locked_vertices,n-locked_vertices);
    cut_best = INT_MAX;
    if (branch (cut))
      FAILED ("GLOBAL_OPT","branch");
  }
  else
  { if (party_lib(n,vertex_w,NULL,NULL,NULL,edge_p,edge,edge_w,p,part,&cut_best,0,NULL,NULL,NULL,NULL,0,0))
      FAILED ("GLOBAL_OPT", "party_lib");
    if (Output > 0)
      printf("GLOBAL_OPT starts with cut %d...\n",cut_best);
    for (i=0; i<n; i++)
      Stack[++Stack_head] = i;
    i = Stack[Stack_head--];
    status[i] = 0;
    weight[0] -= (vertex_w?vertex_w[i]:1);
    for (j=Edge_p[i]; j<Edge_p[i+1]; j++)
      lb[Edge[j]][0] = max_lb[Edge[j]] = (Edge_w?Edge_w[j]:1);
    if (branch (0))
      FAILED ("GLOBAL_OPT","branch");
  }

  FREE(status,int,n);
  FREE(weight,VW,p);
  FREE(max_lb,int,n);
  FREE(lb[0],int,n*p);
  FREE(lb,int*,n);
  FREE(Stack,int,n);
  if (Output > 0)
    printf ("GLOBAL_OPT...#branches:%d #Solutions:%d(%d) Best cutsize:%d\n",branches,solutions,solutions_best,cut_best);
  return 0;
}

int global_lin (int n, VW *vertex_w, int p, int *part)
{ int   i, number=0;
  VW vertex_weight, part_weight=0, max_part_weight=0;

  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);
  
  for (i=0; i<n; i++)
  { vertex_weight = (vertex_w)?vertex_w[i]:1;
    if (part_weight+vertex_weight > max_part_weight) 
    { number++;
      part_weight = 0;
    }
    part[i] = number;
    part_weight += vertex_weight;
  }
  return 0; 
}

int global_sca (int n, VW *vertex_w, int p, int *part)
{ int     i,  number;
  VW   *weight, vertex_weight, max_part_weight=0;

  CALLOC ("GLOBAL_SCATTERED",weight,VW,p);
  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);

  for (i=0; i<n; i++)
  { number = i%p;
    vertex_weight = (vertex_w)?vertex_w[i]:1;
    while (weight[number]+vertex_weight > max_part_weight)
      number = (number+1)%p;
    part[i] = number;
    weight[number] += vertex_weight;
  }
  FREE(weight,VW,p);
  return 0;
}

int global_ran (int n, VW *vertex_w, int p, int *part)
{ int   i, number, vertex, *left_vertices, cur_part=0;
  VW    *weight, vertex_weight, max_part_weight=0;

  MALLOC ("GLOBAL_RANDOM",left_vertices,int,n);
  for (i=0; i<n; i++)
    left_vertices[i] = i;
  CALLOC ("GLOBAL_RANDOM",weight,VW,p);
  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);
  if (!srand_set)
  { srand ((long)RANDOM_SEED);
    srand_set = 1;
  }

  for (i=n; i>0; i--)
  { number = rand()%i;
    vertex = left_vertices[number];
    left_vertices[number] = left_vertices[i-1];
    vertex_weight = (vertex_w)?vertex_w[vertex]:1;
    if (weight[cur_part]+vertex_weight > max_part_weight)
      cur_part++;
    part[vertex] = cur_part;
    weight[cur_part] += vertex_weight;
  }
  FREE(left_vertices,int,n);
  FREE(weight,VW,p);
  return 0;
}

int global_gbf (int n, VW *vertex_w, int *edge_p, int *edge, 
	int *edge_w, int p, int *part)
{ int           i, j, k, deg_min, deg_new, vertex=0, n_left=n, done=0;
  VW		part_weight=0, min_part_weight=0;
  int		*queue, front_queue=0, end_queue=0;

  MALLOC ("GLOBAL_GBF",queue,int,n);
  MIN_PART_WEIGHT(min_part_weight,n,vertex_w,p);

  deg_min = INT_MAX;
  for (i=0; i<n; i++)
  { part[i] = -1;
    deg_new = 0;
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
      deg_new += ((edge_w)?edge_w[j]:1);
    if (deg_new < deg_min)
    { vertex = i;
      deg_min = deg_new;
  } }
  end_queue = 1;
  queue[0] = vertex;

  for (i=0; i<p; i++)
  { part_weight = 0;
    if (front_queue < end_queue)
    { deg_min = INT_MAX;
      for (k=front_queue; k<end_queue; k++)
      { deg_new = 0;
	for (j=edge_p[queue[k]]; j<edge_p[queue[k]+1]; j++)
	  if (part[edge[j]] < 0)
	    deg_new += ((edge_w)?edge_w[j]:1);
        if (deg_new < deg_min)
        { vertex = queue[k];
	  deg_min = deg_new;
	}
	part[queue[k]] = -1;
      }
      front_queue = 0;
      end_queue = 1;
      queue[0] = vertex;
    }

    while (n_left>0 && (part_weight<=min_part_weight || i==p-1))
    { if (front_queue < end_queue)
	vertex = queue[front_queue++];
      else
      { while (done<n && part[done]>=0)
	  done++;
        vertex = done;
      }
      n_left--;
      part[vertex] = i;
      part_weight += ((vertex_w)?vertex_w[vertex]:1);
      for (k=edge_p[vertex]; k<edge_p[vertex+1]; k++)
        if (part[edge[k]] == -1)
	{ queue[end_queue++] = edge[k];
	  part[edge[k]] = -2;
        }
    }
  }
  FREE(queue,int,n);
  return 0;
}

int global_gcf (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part)
{ int		i, j, cur_part, vertex, maxdegree=0, degree_weighted, *diff;
  VW		vertex_weight, part_weight=0, tot_weight=0;
  BUCKETS	*B;
  BUCKET_ELE	*bucket_ele;

  MALLOC ("GLOBAL_GCF",diff,int,n);
  CALLOC ("GLOBAL_GCF",bucket_ele,BUCKET_ELE,n);
  TOT_WEIGHT(tot_weight,n,vertex_w);

  for (i=0; i<n; i++)
  { part[i] = -1;
    bucket_ele[i].element = (void*)i;
    degree_weighted = 0;
    if (edge_w)
    { diff[i] = 0;
      for (j=edge_p[i]; j<edge_p[i+1]; j++)
      { diff[i] -= edge_w[j];
        degree_weighted += abs(edge_w[j]);
      }
    }
    else
    { diff[i] = edge_p[i]-edge_p[i+1];
      degree_weighted = edge_p[i+1]-edge_p[i];
    }
    maxdegree = MAX(maxdegree,degree_weighted);
  }
  if (bucket_cal(&B,-maxdegree, maxdegree))
    FAILED ("GLOBAL_GCF", "bucket_cal");
  if (PARTY_OPT(1)==-1)
    for (i=0; i<n; i++)
      bucket_in(B, &(bucket_ele[i]), PARTY_OPT(diff[i]));

  cur_part=0; 
  for (i=0; i<n; i++)
  { if (part_weight >= (double)tot_weight/p)
    { if (PARTY_OPT(1)==1)
      while (bucket_not_empty(B))
      { vertex = (int)(bucket_max_element(B));
        bucket_del (&(bucket_ele[vertex]));
	for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
	  if (part[edge[j]] == cur_part)
	    diff[vertex] -= ((edge_w)?edge_w[j]:1);
      }
      cur_part++;
      part_weight = 0;
    }
    if (bucket_not_empty(B))
    { vertex = (int)(bucket_max_element(B));
      bucket_del (&(bucket_ele[vertex]));
    }
    else
    { vertex = 0;
      for (j=0; j<n; j++)
	if (part[j]==-1 && (part[vertex]!=-1 || diff[j]>diff[vertex]))
          vertex = j;
    }
    vertex_weight = (vertex_w)?vertex_w[vertex]:1; 
    part[vertex] = cur_part;
    part_weight += vertex_weight;
    for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
      if (part[edge[j]] == -1)
      { diff[edge[j]] += (2*((edge_w)?edge_w[j]:1));
        if (buckets(bucket_ele[edge[j]]))
          bucket_new_key(&(bucket_ele[edge[j]]), PARTY_OPT(diff[edge[j]]));
        else
	  bucket_in(B, &(bucket_ele[edge[j]]), PARTY_OPT(diff[edge[j]]));
      }
  }
  FREE(diff,int,n);
  FREE(bucket_ele,BUCKET_ELE,n);
  bucket_free (B);
  return 0;
}

static float *axis[3];
static int compare (const int *a, const int *b)
{ if (axis[0][(*a)] < axis[0][(*b)])
    return -1;
  else if (axis[0][(*a)] > axis[0][(*b)])
    return 1;
  else if (axis[1])
  { if (axis[1][(*a)] < axis[1][(*b)])
      return -1;
    else if (axis[1][(*a)] > axis[1][(*b)])
      return 1;
    else if (axis[2])
    { if (axis[2][(*a)] < axis[2][(*b)])
        return -1;
      else if (axis[2][(*a)] > axis[2][(*b)])
        return 1;
    }
  }
  return 0;
}
int global_coo (int n, VW *vertex_w, float *x, float *y, float *z,
	int p, int *part)
{ int   i, number, *vertices;
  float x_min, x_max, y_min, y_max, z_min, z_max;
  VW	vertex_weight, part_weight=0, max_part_weight=0;

  MAX_PART_WEIGHT(max_part_weight,n,vertex_w,p);

  if (!x)
    ERROR ("GLOBAL_COORDINATE", "no x coordinates")
  else if (!y && z)
    ERROR ("GLOBAL_COORDINATE", "z coordinates set, but not y coordinates");
  MALLOC ("GLOBAL_COORDINATE",vertices,int,n);
  for (i=0; i<n; i++)
    vertices[i] = i;

  axis[0] = x;
  axis[1] = y;
  axis[2] = z;
  x_max = x_min = x[0];
  for (i=1; i<n; i++)
  { x_min = MIN(x_min,x[i]);
    x_max = MAX(x_max,x[i]);
  }
  if (y)
  { y_min = y_max = y[0];
    for (i=1; i<n; i++)
    { y_min = MIN(y_min,y[i]);
      y_max = MAX(y_max,y[i]);
    }
    if (y_max-y_min > x_max - x_min)
    { axis[0] = y;
      axis[1] = x;
    }
    if (z)
    { z_max = z_min = z[0];
      for (i=1; i<n; i++)
      { z_min = MIN(z_min,z[i]);
        z_max = MAX(z_max,z[i]);
      }
      if (z_max-z_min > x_max-x_min  &&  z_max-z_min > y_max-y_min)
      { axis[2] = axis[1];
        axis[1] = axis[0];
        axis[0] = z;
      }
      else if (z_max-z_min > x_max-x_min  ||  z_max-z_min > y_max-y_min)
      { axis[2] = axis[1];
        axis[1] = z;
      }
    }
  }

  qsort (vertices, n, sizeof(int), (int(*)(const void*, const void*))compare);

  number=0;
  for (i=0; i<n; i++)
  { vertex_weight = (vertex_w)?vertex_w[vertices[i]]:1;
    if (part_weight+vertex_weight > max_part_weight)
    { number++;
      part_weight = 0;
    }
    part[vertices[i]] = number;
    part_weight += vertex_weight;
  }
  FREE(vertices,int,n);
  return 0;
}

int global_fil (int n, int p, int *part, char *filename)
{ int   i;
  FILE  *f;

  if ((f = fopen(filename, "r")) == NULL)
  { fprintf(stderr, "GLOBAL_FILE ERROR...no valid filename %s in global_file.\n", filename);
    return 1;
  }
  for (i=0; i<n; i++)
  { if (fscanf(f, "%d", &(part[i])) == EOF)
    { fprintf(stderr, "GLOBAL_FILE ERROR...Partition file %s is too short!\n", filename);
      return 1;
    }
    else if (part[i] < 0  ||  part[i] >=p)
    { fprintf(stderr, "GLOBAL_FILE ERROR...wrong number %d in partition file %s. Range:[0,%d]\n", part[i],filename,p-1);
      return 1;
    }
  }
  if (fscanf(f, "%d", &i) != EOF)
  { fprintf(stderr, "GLOBAL_FILE ERROR...partition file %s is too long!\n", filename);
    return 1;
  }
  if (fclose(f))
    FAILED ("GLOBAL_FILE", "fclose");
  return 0;
}
