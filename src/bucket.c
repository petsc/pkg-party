/**********************************************************\
*  PARTY PARTITIONING LIBRARY            bucket.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

int bucket_cal (BUCKETS** buckets, int from, int to)  
{ CALLOC ("BUCKET_ALLOC",*buckets,BUCKETS,1);
  (*buckets)->total_buckets = to-from+1;
  (*buckets)->const_add = -from;
  CALLOC ("BUCKET_ALLOC",(*buckets)->bucket,BUCKET_ELE*,(*buckets)->total_buckets);
  (*buckets)->max_bucket = -1;
  return 0;
}

void bucket_free (BUCKETS *buckets)
{ FREE(buckets->bucket,BUCKET_ELE*,buckets->total_buckets);
  FREE(buckets,BUCKETS,1);
}

void bucket_in (BUCKETS *buckets, BUCKET_ELE *element, int key)
{ if (key < -buckets->const_add  ||  key > buckets->total_buckets-buckets->const_add-1)
  { printf("BUCKET_INSERT ERROR...key %d not in [%d,%d]!\n",key,-buckets->const_add,buckets->total_buckets-buckets->const_add-1);
    exit(1);
  }
  element->buckets = buckets;
  element->next = *((element->prev)=(&(buckets->bucket[(key+=buckets->const_add)])));
  if (element->next)
    element->next->prev = &(element->next);
  buckets->bucket[key] = element;
  if (key > buckets->max_bucket)
    buckets->max_bucket = key;
}

void bucket_del (BUCKET_ELE *element)
{ (*(element->prev)) = element->next;
  if (element->next)
    element->next->prev = element->prev;
  else
    while (element->buckets->max_bucket>=0  &&  !(element->buckets->bucket[element->buckets->max_bucket]))
      (element->buckets->max_bucket)--;
  element->buckets = NULL;
}

void bucket_new_key (BUCKET_ELE *element, int key)
{ BUCKETS  *buckets=element->buckets;

  if (key < -buckets->const_add  ||  key > buckets->total_buckets-buckets->const_add-1)
  { printf("BUCKET_INSERT ERROR...key %d not in [%d,%d]!\n",key,-buckets->const_add,buckets->total_buckets-buckets->const_add-1);
    exit(1);
  }
  (*(element->prev)) = element->next;
  if (element->next)
    element->next->prev = element->prev;
  else
    while (buckets->max_bucket>=0  &&  !(buckets->bucket[buckets->max_bucket]))
      buckets->max_bucket--;
  element->next = *((element->prev)=(&(buckets->bucket[(key+=buckets->const_add)])));
  if (element->next)
    element->next->prev = &(element->next);
  buckets->bucket[key] = element;
  if (key > buckets->max_bucket)
    buckets->max_bucket = key;
}

int bucket_print (BUCKETS *buckets)
{ BUCKET_ELE 	*element;
  int           i, max=-1, number_elements, total_number_elements=0;

  printf ("%d Buckets (add %d, max %d)\n",buckets->total_buckets, buckets->const_add, buckets->max_bucket);
  for (i=0; i<buckets->total_buckets; i++)
  { number_elements = 0;
    if (buckets->bucket[i])
      max = i;
    element = buckets->bucket[i];
    while (element)
    { if (*(element->prev) != element)
        ERROR ("BUCKET_PRINT", "wrong prev-pointer in buckets");
      element = element->next;
      number_elements++;
    }
    total_number_elements += number_elements;
    if (number_elements)
      printf ("bucket %d:%d\n", i-buckets->const_add, number_elements);
  }
  printf ("Total elements: %d\n", total_number_elements);
  if (max != buckets->max_bucket)
    ERROR ("BUCKET_PRINT", "wrong max in buckets");
  return 0;
}

void bucket_in_mod (BUCKETS *buckets, BUCKET_ELE *element, int key, 
	int mod)
{ key = key%mod;
  if (bucket_empty(buckets))
    buckets->max_bucket = key+buckets->const_add;
  element->buckets = buckets;
  element->next = *((element->prev)=(&(buckets->bucket[(key+=buckets->const_add)])));
  if (element->next)
    element->next->prev = &(element->next);
  buckets->bucket[key] = element;
}

BUCKET_ELE* bucket_out_mod (BUCKETS *buckets, int mod)
{ int i;
  BUCKET_ELE *element=NULL;

  if (bucket_not_empty(buckets))
  { for (i=0; i<mod && !(buckets->bucket[buckets->max_bucket]); i++)
      (buckets->max_bucket) = (buckets->max_bucket+mod-1)%mod;
    if (buckets->bucket[buckets->max_bucket])
    { element = buckets->bucket[buckets->max_bucket];
      (*(element->prev)) = element->next;
      if (element->next)
        element->next->prev = element->prev;
      element->buckets = NULL;
    }
  }
  return element;
}

void bucket_new_key_mod (BUCKET_ELE *element, int key, int mod)
{ BUCKETS  *buckets=element->buckets;

  key = key%mod;
  (*(element->prev)) = element->next;
  if (element->next)
    element->next->prev = element->prev;
  element->next = *((element->prev)=(&(buckets->bucket[(key+=buckets->const_add)])));
  if (element->next)
    element->next->prev = &(element->next);
  buckets->bucket[key] = element;
}

