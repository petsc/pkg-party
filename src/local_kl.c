/**********************************************************\
*  PARTY PARTITIONING LIBRARY            local_kl.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

#define KL_BAD_STEPS	(total_weight/4) /*Allowed steps without improvement*/

static long	t=0, t_init=0, t_pre=0, t_run=0, t_step=0;

void kl_times_start (void)
{ t=t_pre=t_run=t_step=0;
}

void kl_times_output (void)
{ if(t_pre||t_run||t_step)
  { printf("      KL pre            : %.2f\n",(double)t_pre /1000);
    printf("      KL run            : %.2f\n",(double)t_run /1000);
    printf("      KL step           : %.2f\n",(double)t_step/1000);
  }
}

#define gain(V) \
{ int j; for(j=0;j<p;j++) edges[j]=0; \
  orient[(V)]=(part[(V)]+1)%p; \
  for(j=edge_p[(V)];j<edge_p[(V)+1];j++) \
    edges[part[edge[j]]] += (edge_w?edge_w[j]:1); \
  in[(V)] = edges[part[(V)]]; \
  for(j=0;j<p;j++) \
    if (j!=part[(V)] && PARTY_OPT(edges[j])>PARTY_OPT(edges[orient[(V)]])) \
      orient[(V)] = j; \
  ex[(V)]=edges[orient[(V)]]; \
}

#define gain_check(V) \
{ int j, orient_=orient[(V)], in_=in[(V)], ex_=ex[(V)]; \
  gain((V)); \
  if(edges[orient_]!=edges[orient[(V)]]||in_!=in[(V)]||ex_!=ex[(V)]) \
  { printf("WARNING:vertex %d part:%d orient:%d,%d in:%d,%d ex:%d,%d\n",(V),part[(V)],orient_,orient[(V)],in_,in[(V)],ex_,ex[(V)]); \
    for (j=0; j<p; j++) printf("edges[%d]=%d\n",j,edges[j]); \
    return 1; \
  } \
}

int local_kl (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int p, int *part, int Output)
{ int		i, j, vertex, neig, *in, *ex, *orient, *locked, *locked_list,
		*edges, round=0, cutsize=0, best_cutsize, degree_weigted,
		maxdegree=0;
  VW		max_vertex_weight=1, total_weight=n, max_part_weight,
  		max_weight, *part_weight, best_max_weight=0;
  BUCKETS	**B;
  BUCKET_ELE	*bucket_ele;
  INIT_TIME();

  CALLOC ("LOCAL_KL",in,int,n);
  CALLOC ("LOCAL_KL",ex,int,n);
  CALLOC ("LOCAL_KL",orient,int,n);
  CALLOC ("LOCAL_KL",locked,int,n);
  CALLOC ("LOCAL_KL",locked_list,int,n);
  CALLOC ("LOCAL_KL",edges,int,p);
  CALLOC ("LOCAL_KL",part_weight,VW,p); 
  CALLOC ("LOCAL_KL",B,BUCKETS*,p);
  CALLOC ("LOCAL_KL",bucket_ele,BUCKET_ELE,n);

  WEIGHTS(total_weight,max_part_weight,max_vertex_weight,n,vertex_w,p);

  for (i=0; i<n; i++)
  { bucket_ele[i].element = (void*)i;
    if (part[i]<0 || part[i]>=p)
    { fprintf(stderr, "LOCAL_KL ERROR...part[%d]=%d not in range 0..%d!\n",i,part[i],p-1);
      return 1;
    }
    part_weight[part[i]] += (vertex_w?vertex_w[i]:1);
    degree_weigted = 0;
    for (j=edge_p[i]; j<edge_p[i+1]; j++)
      degree_weigted += ((edge_w)?abs(edge_w[j]):1);
    if (degree_weigted > maxdegree)
       maxdegree = degree_weigted;
  }
  if (Output > 0)
  { max_weight = 0;
    for (i=0; i<p; i++)
    { max_weight = MAX(max_weight,part_weight[i]);
      if (part_weight[i] > max_part_weight)
      { WARNING ("LOCAL_KL","unbalanced initial partition");
	printf("weight of part %d = %.2f too high (<%.2f)!\n",i,(double)(part_weight[i]),(double)max_part_weight);
      }
    }
    printf("KL START... max. weight: %.2f (<%.2f)  cutsize: %3d\n", (double)max_weight, (double)max_part_weight, cut_size(n,edge_p,edge,edge_w,part));
  }
  best_cutsize = cutsize = cut_size(n,edge_p,edge,edge_w,part);
  for (i=0; i<p; i++)
  { if (bucket_cal(&(B[i]),-maxdegree, maxdegree))
      FAILED ("LOCAL_KL", "bucket_cal");
    best_max_weight = MAX(best_max_weight,part_weight[i]);
  }
  for (i=0; i<n; i++)
  { gain(i);
    if (ex[i]>0 || PARTY_OPT(1)==-1)
      bucket_in(B[part[i]],&(bucket_ele[i]),PARTY_OPT(ex[i]-in[i]));
  }
  ADD_NEW_TIME(t_pre);

  do 
  { int		step=0, akt_cutsize=best_cutsize, number_locked=0, 
		best_locked=0, sour, dest, vertex_j;
    VW		no_better_steps=0, vertex_weight, max_sour, max_j;
    
    round++;
    cutsize = best_cutsize;
    if (Output > 3)
      printf("ROUND %d:\nSTEP VERTEX  PARTS MAX_WGT CHANGE CUTSIZE\n",round);
    if (Output > 4)
      for (sour=0; sour<p; sour++)
	bucket_print(B[sour]);
    if (Xpart>=2)
    { sprintf (xpart_c, "kl round %d:",round);
      XPART(2,"LOCAL_KL",part);
    }

    ADD_NEW_TIME(t_run);

    while (no_better_steps<KL_BAD_STEPS)
    { step++;
      for(sour=0; sour<p && bucket_empty(B[sour]); sour++);
      if (sour == p)
	break;
      for (j=sour+1; j<p; j++)
        if (bucket_not_empty(B[j]))
	{ if (part_weight[j] >= max_part_weight)
	  { if (part_weight[sour] >= max_part_weight)
	    { vertex = (int)(bucket_max_element(B[sour]));
	      vertex_weight = (vertex_w?vertex_w[vertex]:1);
	      max_sour = MAX(part_weight[sour]-vertex_weight,part_weight[orient[vertex]]+vertex_weight);
	      vertex_j = (int)(bucket_max_element(B[j]));
	      vertex_weight = (vertex_w?vertex_w[vertex_j]:1);
	      max_j = MAX(part_weight[j]-vertex_weight,part_weight[orient[vertex_j]]+vertex_weight);
              if (max_j<max_sour || (max_j==max_sour && PARTY_OPT(ex[vertex_j]-in[vertex_j])>PARTY_OPT(ex[vertex]-in[vertex])))
	        sour = j;
            }
	    else
	      sour = j;
	  }
	  else
	  { if (part_weight[sour] < max_part_weight)
	    { vertex = (int)(bucket_max_element(B[sour]));
	      vertex_weight = (vertex_w?vertex_w[vertex]:1);
	      max_sour = MAX(part_weight[sour]-vertex_weight,part_weight[orient[vertex]]+vertex_weight);
	      vertex_j = (int)(bucket_max_element(B[j]));
	      vertex_weight = (vertex_w?vertex_w[vertex_j]:1);
	      max_j = MAX(part_weight[j]-vertex_weight,part_weight[orient[vertex_j]]+vertex_weight);
	      if (max_j >= max_part_weight)
	      { if (max_sour>=max_part_weight && (max_j<max_sour || (max_j==max_sour && PARTY_OPT(ex[vertex_j]-in[vertex_j])>PARTY_OPT(ex[vertex]-in[vertex]))))
		  sour = j;
	      }
	      else if (max_sour>=max_part_weight || (PARTY_OPT(ex[vertex_j]-in[vertex_j])>PARTY_OPT(ex[vertex]-in[vertex]) || (PARTY_OPT(ex[vertex_j]-in[vertex_j])==PARTY_OPT(ex[vertex]-in[vertex]) && max_j<max_sour)))
		sour = j;
	    }
	  }
	}

      vertex = (int)(bucket_max_element(B[sour]));
      bucket_del (&(bucket_ele[vertex]));
      locked[vertex] = part[vertex]+1;
      locked_list[number_locked++] = vertex;
      dest = part[vertex] = orient[vertex];
      akt_cutsize -= (ex[vertex]-in[vertex]);
      if (p == 2)
      { INT_CHANGE(in[vertex],ex[vertex]);
	orient[vertex] = sour;
      }
      else
	gain(vertex);
      part_weight[sour] -= (vertex_w?vertex_w[vertex]:1);
      part_weight[dest] += (vertex_w?vertex_w[vertex]:1);
      no_better_steps += (vertex_w?vertex_w[vertex]:1);

      for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
	if (!locked[neig=edge[j]])
        { if (p == 2)
	  { if (part[neig] == sour)
	    { in[neig] -= (edge_w?edge_w[j]:1);
	      ex[neig] += (edge_w?edge_w[j]:1);
	      orient[neig] = dest;
	    }
	    else
	    { in[neig] += (edge_w?edge_w[j]:1);
	      ex[neig] -= (edge_w?edge_w[j]:1);
	    }
	  }
	  else
	    gain(neig);
	  if (buckets(bucket_ele[neig]))
	  { if (ex[neig]==0 && PARTY_OPT(1)==1)
	      bucket_del (&(bucket_ele[neig]));
            else
	      bucket_new_key(&(bucket_ele[neig]), PARTY_OPT(ex[neig]-in[neig]));
          }
	  else if (ex[neig]>0 || PARTY_OPT(1)==-1)
	    bucket_in(B[part[neig]],&(bucket_ele[neig]),PARTY_OPT(ex[neig]-in[neig]));
        }

      max_weight = 0;
      for (i=0; i<p; i++)
	max_weight = MAX(max_weight,part_weight[i]);
      if ((best_max_weight>max_part_weight && max_weight<best_max_weight) ||
	  (max_weight<=max_part_weight && PARTY_OPT(akt_cutsize)<PARTY_OPT(best_cutsize)))
      { best_locked = number_locked;
        best_cutsize = akt_cutsize;
	best_max_weight = max_weight;
        if (Output > 3)
	  printf ("KL New best cutsize : %d\n",best_cutsize);
        no_better_steps = 0;
      }

      if (Output > 3)
      { max_weight = 0;
	for (i=0; i<p; i++)
	  max_weight = MAX(max_weight,part_weight[i]);
        printf ("%4d %6d %2d->%2d %7.2f %6d %7d\n",step,vertex,sour,dest,(double)max_weight,akt_cutsize-cutsize,akt_cutsize); 
      }
      if (Xpart>=3)
      { sprintf (xpart_c, "kl: %d locked",vertex);
        XPART(3,"LOCAL_KL",part);
      }
    }

    ADD_NEW_TIME(t_step);

    while (number_locked != best_locked)
    { vertex = locked_list[--number_locked];
      sour = part[vertex];
      dest = locked[vertex]-1;
      part[vertex] = dest;
      if (p == 2)
      { INT_CHANGE(in[vertex],ex[vertex]);
        orient[vertex] = sour;
      }
      else
	gain(vertex);
      locked[vertex] = 0;
      part_weight[sour] -= (vertex_w?vertex_w[vertex]:1);
      part_weight[dest] += (vertex_w?vertex_w[vertex]:1);
      if (ex[vertex]>0 || PARTY_OPT(1)==-1)
	bucket_in(B[part[vertex]],&(bucket_ele[vertex]),PARTY_OPT(ex[vertex]-in[vertex]));
      for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
        if (!locked[neig=edge[j]])
	{ if (p == 2)
	  { if (part[neig] == sour)
	    { in[neig] -= (edge_w?edge_w[j]:1);
	      ex[neig] += (edge_w?edge_w[j]:1);
	      orient[neig] = dest;
	    }
	    else
	    { in[neig] += (edge_w?edge_w[j]:1);
	      ex[neig] -= (edge_w?edge_w[j]:1);
	    }
	  }
	  else
	    gain(neig);
	  if (buckets(bucket_ele[neig]))
	  { if (ex[neig]==0 && PARTY_OPT(1)==1)
	      bucket_del (&(bucket_ele[neig]));
	    else
	      bucket_new_key(&(bucket_ele[neig]),PARTY_OPT(ex[neig]-in[neig]));
	  }
	  else if (ex[neig]>0 || PARTY_OPT(1)==-1)
	    bucket_in(B[part[neig]],&(bucket_ele[neig]),PARTY_OPT(ex[neig]-in[neig]));
        }
    }
    while (number_locked)
    { vertex = locked_list[--number_locked];
      locked[vertex] = 0;
      if (p == 2)
      { for (j=edge_p[vertex]; j<edge_p[vertex+1]; j++)
	  if (locked[neig=edge[j]])
	  { if (part[neig] == part[vertex])
	    { in[neig] += (edge_w?edge_w[j]:1);
	      ex[neig] -= (edge_w?edge_w[j]:1);
	    }
	    else
	    { in[neig] -= (edge_w?edge_w[j]:1);
	      ex[neig] += (edge_w?edge_w[j]:1);
	    }
          }
      }
      else
	gain(vertex);
      if (ex[vertex]>0 || PARTY_OPT(1)==-1)
	bucket_in(B[part[vertex]],&(bucket_ele[vertex]),PARTY_OPT(ex[vertex]-in[vertex]));
    }

    if (Output > 2)
    { printf ("KL New Cutsize         : %d\n", best_cutsize);
      for (i=0; i<n; i++)
        gain_check (i);
    }
  } while (PARTY_OPT(best_cutsize) < PARTY_OPT(cutsize));

  if (Output > 0)
  { max_weight = 0;
    for (i=0; i<p; i++)
      max_weight = MAX(max_weight,part_weight[i]);
    printf("KL END  ... max. weight: %.2f (<%.2f)  cutsize: %3d \n",(double)max_weight,(double)max_part_weight,cut_size(n,edge_p,edge,edge_w,part));
  }
  FREE(in,int,n);
  FREE(ex,int,n);
  FREE(orient,int,n);
  FREE(locked,int,n);
  FREE(locked_list,int,n);
  FREE(edges,int,p);
  FREE(part_weight,VW,p);
  for (i=0; i<p; i++)
    bucket_free(B[i]);
  FREE(B,BUCKETS*,p);
  FREE(bucket_ele,BUCKET_ELE,n);

  ADD_NEW_TIME(t_run);
  END_TIME();
  return 0;
}
