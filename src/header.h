/**********************************************************\
*  PARTY PARTITIONING LIBRARY            header.h
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include "../party_lib.h"

#define MIN(A,B)                (((A)<(B))?(A):(B))
#define MAX(A,B)                (((A)>(B))?(A):(B))
#define INT_CHANGE(A,B)         {int   _C_=(A);(A)=(B);(B)=_C_;}
#define RANDOM_SEED             /* 123456789 */ (time ((time_t*)NULL))
extern int srand_set;

#define PARTY_OPT(A)		(A) /*(A) for MIN, (-1*(A)) for MAX*/


/* ---------- COARSENING */
extern int matching_lam (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int redl, int *matching, int *red_n);
extern int matching_opt_w3 (int n, VW *vertex_w, int *edge_p, int *edge,
	int* edge_w, int *matching, int* red_n, int redl);
extern int graph_reduce (int n, VW *vertex_w, float *x, float *y, float *z,
	int *edge_p, int *edge, int *edge_w,
	int red_n, VW *red_vertex_w, float *red_x, float *red_y, float *red_z,
	int *red_edge_p, int *red_edge, int *red_edge_w,
	int *matching, int *red_to, int *superedge);


/* ----------- XPART */
extern int     Xpart, *id;
extern char    xpart_c[100];
#ifdef WORK
extern int      xpart (char *text, int cut, int n, int *id, int *color);
extern void     xpart_exit ();
#ifdef VISTOOL
#define XPART(X,P,PA) \
	{ printf("%s\n",xpart_c); \
	  if (Xpart>=X && update_party(xpart_c,cut_size(n,edge_p,edge,edge_w,PA),n,id,PA,0,0)) \
	    FAILED (P, "xpart"); }
#define XPART_EXIT()  { if (Xpart>0) xpart_exit (); }
#else
#define XPART(X,P,PA) \
	{ if (Xpart>=X && xpart(xpart_c,cut_size(n,edge_p,edge,edge_w,PA),n,id,PA)) \
	    FAILED (P, "xpart"); }
#define XPART_EXIT()  { if (Xpart>0) xpart_exit (); }
#endif
#else
#define XPART(X,P,PA) {}
#define XPART_EXIT()  {}
#endif


/* ----------- TIME */
#if defined(CLOCK)
#define INIT_TIME()      {if(!t_init)t=clock()/1000;t_init++;}
#define ADD_NEW_TIME(T)  {T-=t; T+=(t=clock()/1000);}
#elif !defined(WIN)
#include <sys/time.h>
#include <sys/resource.h>
extern struct rusage	Rusage;
extern int getrusage (int who, struct rusage *rusage);
#define INIT_TIME()  	{if(!t_init) \
			 { getrusage(RUSAGE_SELF,&Rusage); \
		           t=Rusage.ru_utime.tv_sec*1000+ \
			     Rusage.ru_utime.tv_usec/1000; } \
			 t_init++;}
#define ADD_NEW_TIME(T) {T-=t; getrusage(RUSAGE_SELF,&Rusage); \
			 T+=(t=Rusage.ru_utime.tv_sec*1000+ \
			       Rusage.ru_utime.tv_usec/1000);}
#else
#define INIT_TIME()	{}
#define ADD_NEW_TIME(T)	{}
#endif
#define END_TIME()	{t_init--;}


/* ----------- Memory allocation */
extern int alloc_memory;
extern int max_memory;

#define CALLOC(P,V,T,N) \
{ if (!((V) = (T*)calloc((unsigned)(N),sizeof(T)))) \
  { fprintf(stderr, P); \
    fprintf(stderr, " ERROR...no memory!\n"); \
    fprintf(stderr, "%d + %d additional memory is too much.\n",alloc_memory,(unsigned)(N) * sizeof(T)); \
    return 1; \
  } \
  alloc_memory += (unsigned)(N) * sizeof(T); \
  if (alloc_memory > max_memory) \
    max_memory = alloc_memory; \
}
#define MALLOC(P,V,T,N) \
{ if (!((V) = (T*)malloc((unsigned)(N)*sizeof(T)))) \
  { fprintf(stderr, P); \
    fprintf(stderr, " ERROR...no memory!\n"); \
    fprintf(stderr, "%d + %d additional memory is too much.\n",alloc_memory,(unsigned)(N) * sizeof(T)); \
    return 1; \
  } \
  alloc_memory += (unsigned)(N) * sizeof(T); \
  if (alloc_memory > max_memory) \
    max_memory = alloc_memory; \
}
#define FREE(V,T,N) \
{ if (V) \
  { free((char*)V); \
    V = NULL; \
    alloc_memory -= (unsigned)(N) * sizeof(T); \
} }


/* ----------- Balance */
#define WEIGHTS(TW,MPW,MVERW,N,VERW,P) \
	{ if (VERW) \
	  { int i; \
	    (TW) = (MVERW) = 0; \
	    for (i=0; i<(N); i++) \
	    { (TW) += (VERW)[i]; \
	      if ((VERW)[i] > (MVERW)) \
	        (MVERW) = (VERW)[i]; \
	  } } \
  	  else \
	  { (TW)=(N); \
	    (MVERW)=1; \
          } \
	  (MPW)=((TW)+((P)-1)*(MVERW))/(P); \
	}
#define MAX_PART_WEIGHT(MPW,N,VERW,P) \
	{ if (VERW) \
	  { int i; \
	    VW mvw=0; \
	    (MPW) = 0; \
	    for (i=0; i<(N); i++) \
	    { (MPW) += (VERW)[i]; \
	      if ((VERW)[i] > mvw) \
		mvw = (VERW)[i]; \
	    } \
	    (MPW)=((MPW)+((P)-1)*mvw)/(P); \
	  } \
	  else \
	    (MPW)=((VW)(N)+(P)-1)/(P); \
	}
#define MIN_PART_WEIGHT(MPW,N,VERW,P) \
	{ if (VERW) \
	  { int i; \
	    VW mvw=0; \
	    (MPW) = 0; \
	    for (i=0; i<(N); i++) \
	    { (MPW) += (VERW)[i]; \
	      if ((VERW)[i] > mvw) \
		mvw = (VERW)[i]; \
	    } \
	    (MPW)=((MPW)+((P)-1)*mvw)/(P)-mvw; \
	  } \
	  else \
	    (MPW)=((VW)(N)+(P)-1)/(P)-1; \
	}
#define TOT_WEIGHT(TW,N,VERW) \
        { if (VERW) \
          { int i; \
	    (TW) = 0; \
            for (i=0; i<(N); i++) \
              (TW) += (VERW)[i]; \
          } \
          else \
            (TW) = (N); \
        }


/* ----------- Warnings and errors */
#define WARNING(P,T) \
{ fprintf (stderr, P); \
  fprintf (stderr, " WARNING..."); \
  fprintf (stderr, T); \
  fprintf (stderr, "!\n"); \
}
#define ERROR(P,T) \
{ fprintf (stderr, P); \
  fprintf (stderr, " ERROR..."); \
  fprintf (stderr, T); \
  fprintf (stderr, "!\n"); \
  return 1; \
}
#define FAILED(P,T) \
{ fprintf (stderr, P); \
  fprintf (stderr, " ERROR...failed "); \
  fprintf (stderr, T); \
  fprintf (stderr, "!\n"); \
  return 1; \
}


/* ----------- Buckets */
typedef struct BUCKET_ELE     { struct BUCKETS         *buckets;
                                struct BUCKET_ELE      **prev, *next;
                                void                   *element;
	                      } BUCKET_ELE;
typedef struct BUCKETS        { int                    total_buckets;
                                int                    const_add;
                                BUCKET_ELE             **bucket;
			        int                    max_bucket;
		              } BUCKETS;

#define bucket_clear(A)		{ while (bucket_not_empty(A)) \
		                  bucket_del (((A)->bucket[A->max_bucket])); }
#define bucket_empty(A)		(((A)->max_bucket)<0)
#define bucket_not_empty(A)	(((A)->max_bucket)>=0)
#define buckets(A)		((A).buckets)
#define bucket_max_key(A)	((A)->max_bucket-(A)->const_add)
#define bucket_max_element(A)	(((A)->bucket[(A)->max_bucket])->element)

extern int bucket_cal ( 
	BUCKETS **buckets, int from, int to); 
extern void bucket_free (
	BUCKETS *buckets);
extern void bucket_in (
	BUCKETS *buckets, BUCKET_ELE *element, int key);
extern void bucket_del (
	BUCKET_ELE *element);
extern void bucket_new_key (
	BUCKET_ELE *element, int key); 
extern int bucket_print (
	BUCKETS *buckets);

extern void bucket_in_mod (
	BUCKETS *buckets, BUCKET_ELE *element, int key, int mod);
extern BUCKET_ELE* bucket_out_mod (
	BUCKETS *buckets, int mod);
extern void bucket_new_key_mod (
	BUCKET_ELE *element, int key, int mod);
#define bucket_clear_mod(A,M)	{ while (bucket_out_mod(A,M)) {} }
