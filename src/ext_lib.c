/**********************************************************\
*  PARTY PARTITIONING LIBRARY            ext_lib.c
*
*  Robert Preis, Universit\"at Paderborn, Germany
*  preis@hni.uni-paderborn.de
\**********************************************************/

#include "header.h"

#ifdef CHACO

extern int ECHO;
extern int OUTPUT_METRICS;
extern int OUTPUT_TIME;
extern int PRINT_HEADERS;
extern double SRESTOL;
extern int WARNING_EVECS;
extern int FREE_GRAPH;
extern int DEBUG_PARAMS;
extern int KL_METRIC;

extern int interface (
int       nvtxs,          /* number of vertices in full graph */
int      *start,          /* start of edge list for each vertex */
int      *adjacency,      /* edge list data */
int      *vwgts,          /* weights for all vertices */
float    *ewgts,          /* weights for all edges */
float    *x,              /* coordinates for inertial method */
float    *y,
float    *z,
char     *outassignname,  /* name of assignment output file */
char     *outfilename,    /* output file name */
short    *assignment,     /* set number of each vtx (length n) */
int       architecture,   /* 0 => hypercube, d => d-dimensional mesh */
int       ndims_tot,      /* total number of cube dimensions to divide */
int       mesh_dims[3],   /* dimensions of mesh of processors */
double   *goal,           /* desired set sizes for each set */
int       global_method,  /* global partitioning algorithm */
int       local_method,   /* local partitioning algorithm */
int       rqi_flag,       /* should I use RQI/Symmlq eigensolver? */
int       vmax,           /* if so, how many vertices to coarsen down to? */
int       ndims,          /* number of eigenvectors (2^d sets) */
double    eigtol,         /* tolerance on eigenvectors */
long      seed            /* for random graph mutations */
);

#define Chaco_vmax      500
#define Chaco_eigtol    0.001
#define Chaco_seed      7654321
#define Chaco_srestol   10.0

int global_cml (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int p, int *part)
{ int   i, mesh_dim[3], ndims=3;
  short *part_short;
  float	*ewgts=NULL;

  ECHO=OUTPUT_METRICS=OUTPUT_TIME=PRINT_HEADERS=WARNING_EVECS=FREE_GRAPH=DEBUG_PARAMS = 0;
  SRESTOL = Chaco_srestol;
  mesh_dim[0] = p;
  if (p<8)
    ndims--;
  if (p<4)
    ndims--;

  for (i=0; i<edge_p[n]; i++)
    edge[i] += 1;
  if (edge_w)
  { MALLOC ("GLOBAL_MULTILEVEL",ewgts,float,edge_p[n]);
    for (i=0; i<edge_p[n]; i++)
      ewgts[i] = (float)(edge_w[i]);
  }
  MALLOC ("GLOBAL_MULTILEVEL",part_short,short,n);

  if (interface(n,edge_p,edge,vertex_w,ewgts,NULL,NULL,NULL,
		NULL,NULL,
		part_short,
		1,-1,mesh_dim,NULL,
		1,2,1,Chaco_vmax,ndims,Chaco_eigtol,Chaco_seed))
    FAILED ("GLOBAL_MULTILEVEL", "Chaco interface");

  for (i=0; i<edge_p[n]; i++)
    edge[i] -= 1;
  FREE(ewgts,int,edge_p[n]);
  for (i=0; i<n; i++)
    part[i] = part_short[i];
  FREE(part_short,short,n);
  return 0;
}

int global_csm (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int p, int *part)
{ int   i, mesh_dim[3], ndims=3;
  short *part_short;
  float *ewgts=NULL;

  ECHO=OUTPUT_METRICS=OUTPUT_TIME=PRINT_HEADERS=WARNING_EVECS=FREE_GRAPH=DEBUG_PARAMS = 0;
  SRESTOL = Chaco_srestol;
  mesh_dim[0] = p;
  if (p<8)
    ndims--;
  if (p<4)
    ndims--;

  for (i=0; i<edge_p[n]; i++)
    edge[i] += 1;
  if (edge_w)
  { MALLOC ("GLOBAL_SPECTRAL_M",ewgts,float,edge_p[n]);
    for (i=0; i<edge_p[n]; i++)
      ewgts[i] = (float)(edge_w[i]);
  }
  MALLOC ("GLOBAL_SPECTRAL_M",part_short,short,n);

  if (interface(n,edge_p,edge,vertex_w,ewgts,NULL,NULL,NULL,
		NULL,NULL,
		part_short,
		1,-1,mesh_dim,NULL,
		2,2,1,Chaco_vmax,ndims,Chaco_eigtol,Chaco_seed))
    FAILED ("GLOBAL_SPECTRAL_M", "Chaco interface");

  for (i=0; i<edge_p[n]; i++)
    edge[i] -= 1;
  FREE(ewgts,int,edge_p[n]);
  for (i=0; i<n; i++)
    part[i] = part_short[i];
  FREE(part_short,short,n);
  return 0;
}

int global_csl (int n, VW *vertex_w, int *edge_p, int *edge,
	int *edge_w, int p, int *part)
{ int   i, mesh_dim[3], ndims=3;
  short *part_short;
  float *ewgts=NULL;

  ECHO=OUTPUT_METRICS=OUTPUT_TIME=PRINT_HEADERS=WARNING_EVECS=FREE_GRAPH=DEBUG_PARAMS = 0;
  SRESTOL = Chaco_srestol;
  mesh_dim[0] = p;
  if (p<8)
    ndims--;
  if (p<4)
    ndims--;

  for (i=0; i<edge_p[n]; i++)
    edge[i] += 1;
  if (edge_w)
  { MALLOC ("GLOBAL_SPECTRAL_L",ewgts,float,edge_p[n]);
    for (i=0; i<edge_p[n]; i++)
      ewgts[i] = (float)(edge_w[i]);
  }
  MALLOC ("GLOBAL_SPECTRAL_L",part_short,short,n);

  if (interface(n,edge_p,edge,vertex_w,ewgts,NULL,NULL,NULL,
		NULL,NULL,
		part_short,
		1,-1,mesh_dim,NULL,
		2,2,0,Chaco_vmax,ndims,Chaco_eigtol,Chaco_seed))
    FAILED ("GLOBAL_MULTILEVEL", "Chaco interface");

  for (i=0; i<edge_p[n]; i++)
    edge[i] -= 1;
  FREE(ewgts,int,edge_p[n]);
  for (i=0; i<n; i++)
    part[i] = part_short[i];
  FREE(part_short,short,n);
  return 0;
}

int global_cin (int n, VW *vertex_w, float *x, float *y, float *z,
	int p, int *part)
{ int   i, mesh_dim[3], ndims=3;
  short *part_short;

  ECHO=OUTPUT_METRICS=OUTPUT_TIME=PRINT_HEADERS=WARNING_EVECS=FREE_GRAPH=DEBUG_PARAMS = 0;
  SRESTOL = Chaco_srestol;
  mesh_dim[0] = p;
  if (p<8)
    ndims--;
  if (p<4)
    ndims--;

  if (!x)
    ERROR ("GLOBAL_INERTIAL", "no x coordinates");
  MALLOC ("GLOBAL_INERTIAL",part_short,short,n);

  if (interface(n,NULL,NULL,vertex_w,NULL,x,y,z,
	        NULL,NULL,
	        part_short,
	        1,-1,mesh_dim,NULL,
	        3,2,-1,-1,ndims,-1,Chaco_seed))
    FAILED ("GLOBAL_INERTIAL", "Chaco interface");

  for (i=0; i<n; i++)
    part[i] = part_short[i];
  FREE(part_short,short,n);
  return 0;
}

int local_ckl (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part)
{
  int   i, mesh_dim[3], ndims;
  short *part_short;
  float	*ewgts=NULL;

  ECHO=OUTPUT_METRICS=OUTPUT_TIME=PRINT_HEADERS=FREE_GRAPH=DEBUG_PARAMS = 0;
  KL_METRIC = 1;
  mesh_dim[0] = p;
  if (p == 2)
    ndims = 1;
  else if (p == 4)
    ndims = 2;
  else if (p == 8)
    ndims = 3;
  else
  { fprintf(stderr, "LOCAL_CKL ERROR...local_ckl works only for p=2,4,8 and you wanted p=%d!\n",p);
    return 1;
  }

  for (i=0; i<edge_p[n]; i++)
    edge[i] += 1;
  if (edge_w)
  { MALLOC ("LOCAL_CKL",ewgts,float,edge_p[n]);
    for (i=0; i<edge_p[n]; i++)
      ewgts[i] = (float)(edge_w[i]);
  }
  MALLOC ("LOCAL_CKL",part_short,short,n);
  for (i=0; i<n; i++)
    part_short[i] = (short)(part[i]);

  if (interface(n,edge_p,edge,vertex_w,ewgts,NULL,NULL,NULL,
		NULL,NULL,
		part_short,
		1,-1,mesh_dim,NULL,
		7,1,0,0,ndims,0,Chaco_seed))
    FAILED ("LOCAL_CKL", "Chaco interface");

  for (i=0; i<edge_p[n]; i++)
    edge[i] -= 1;
  FREE(ewgts,int,edge_p[n]);
  for (i=0; i<n; i++)
    part[i] = part_short[i];
  FREE(part_short,short,n);
  return 0;
}

#endif   /* CHACO */



#ifdef METIS

typedef int idxtype;
/* pmetis.c */
extern void METIS_PartGraphRecursive(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *); 
/* kmetis.c */
extern void METIS_PartGraphKway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *); 

int global_pme (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w, 
	int p, int *part)
{ int   wgtflag=0, numflag=0, options=0, edgecut;

  if (vertex_w)
    wgtflag += 2;
  if (edge_w)
    wgtflag += 1;

  METIS_PartGraphRecursive (&n, edge_p, edge, vertex_w, edge_w, &wgtflag, &numflag, &p, &options, &edgecut, part);

  return 0;
}

int global_kme (int n, VW *vertex_w, int *edge_p, int *edge, int *edge_w,
	int p, int *part)
{ int   wgtflag=0, numflag=0, options=0, edgecut;

  if (vertex_w)
    wgtflag += 2;
  if (edge_w)
    wgtflag += 1;

  METIS_PartGraphKway (&n, edge_p, edge, vertex_w, edge_w, &wgtflag, &numflag, &p, &options, &edgecut, part);

  return 0;
}
#endif   /* METIS */
